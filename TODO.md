- CSV output: Format timestamp as ISO 8601
    e.g. 2016-12-24T13:01:02,123Z (maybe without timezone zero)
         decimal fraction is . or , according to ISO 8601:2004 with preference to ,
- CSV output: Format duration as ISO 8601
    e.g. PT0.021S (Period Time 0.021 Seconds)
- SEND: rtrim( SR )
- Add status beforeTransaction


TransactionBeginParser reads cacv as long.
In fact it's the hex-representation of an uint32_t.
==> create template class with for hex-numbers with iostream support.

enum ByteOrder { littleEndian, bigEndian };

template <typename IntType>
class HexInt {
	IntType value;
	ByteOrder byteOrder;

	HexInt(string, ByteOrder);

	string toHex(ByteOrder);
}


HexInt i(1, littleEndian);	// i.value = 0x0100'0000
i.toHex(littleEndian);		// 01000000
i.toHex(bigEndian);			// 00000001