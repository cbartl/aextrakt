## events.csv

Data about all sent events (aka messages). With this data it is possible to create a call-graph of
event workflow.

One line per sent event.

Description of all variables (columns):


### Event Information

#### eventId
int32

The numeric event id (called MsgID in trace). eventId alone is not a unique number.
Together with *queue* it is somehow unique. Only eventId 0 can occur multiple times.

#### name
string, factor

Name of the event, e.g. CALECHK, MQBUSY, ...

### Receiver Information

#### receiverSR
string, factor

The server routine the event is sent to, e.g. UCMAIN_R, UCGENX_R, ...

#### receiverProcess
string, factor

The process the event is sent to.
- For event to CP: Name of the CP process, e.g. SQL9_MY#CP001
- For event to UI or Java Api: "proxy" CP + its connection id,<br>
  e.g. *CP001#00000006
- For event to WP: *SERVER
- For event to an agent: Agent name


#### queue
string, factor

The MQ-table the event was stored in.
INTERNAL for so called "internal messages", sent to all WPs via TCP/IP by PWP.


#### timestamp
timestamp in ISO 8601 compatible format "2016-11-15 11:47:16.866"

Time when the event was sent.

#### eventDetail
string

To be improved. Currently unusable.

#### client
int16

The client from which the event is sent.

#### cacv, bacv
currently string, will change

Internal id of additional data sent with the event.
cacv is data supposed to be read by the callee.
bacv is data saved by the caller in order to re-read after the callee returns.

As there is no uniform format across platforms and processes the value is stored as string.
This will be improved later to store all acv as numeric value.


#### type
char, factor

Event type:
- I - internal
- W - WP
- ? - Somehow CP related :-)

Don't feel bad if you cannot find a use-case for that variable.


#### prio
int16

Each message has a priority which affects its position in the queue.
Low value means higher priority.

#### clientPrio
int16

Not sure what this is.


#### userid
int32

If this event is sent in the course of any actions triggered by a user the OH_Idnr of this
user is stored.


### Sender Information

#### senderEvent
int32

If the event is sent while processing another event (or transaction) this Event-Id
is stored here. This allows to get the call graph.

#### senderSR
string, factor

Server Routine of the sender transaction.

#### senderEventName
string, factor

Event name of the sender transaction

#### filename
string

Name of the original (big) file in which the event was found.
