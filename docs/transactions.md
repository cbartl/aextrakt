## transactions.csv

Metadata of all transactions

One line per transaction.


Description of all variables (columns):

#### filename
string, factor

The name of the tracefile within input path from which this transactin was extraced.

#### eventId
int32

The numeric event id sent by any process/procedure which triggered this transaction.

(Field 'MsgID' from RCV line)


#### SR
string (8 characters), factor

The server routine wich executes the transaction. Possible values are UCMAIN_R, JPEXEC_R, ...


#### xreq
string, factor

Calls to "Dialog Work Process" (from UI or Java Api) may contain more detailled information about
what internal routines are triggered. These are called x-request. If available the name of this
x-request is set here.

Possible values: e.g. CREATE_OX, login, getactivities, getfoldertree, setobject, ...


#### name
string, factor

Similar to *xreq* provides more datailled information which function is called within
a server routine *SR*, e.g. AKTX, TIMER, ...

In "old" WPs and CPs it was limited to 8 characters.
Since introduction of Java-Processes longer values are possible.

This information differs depending on process type and server routine.

#### senderProcess
string, factor

The sender of the received message.
WPs have the format &lt;System>#&lt;WP>. e.g. UC4PROD#WP001 or \*SERVER
Messages can also come from Agents

#### senderSR
string, factor

- If the sender is a WP sender SR contains the sender process, e.g. *SQL9_MY#WP001*.
- If the sender is a CP sender SR contains the CP connection, e.g. *\*CP001#00000006*.
- If it's a so called "internal" event the value is *\*SERVER*.
- If the sender is an agent sender SR contains the agent name.


#### senderDetail
string

Contains some probably little useful detail about the sender process. This and variable *name*
probably needs some improvements.


#### senderSR
string

Contains the server routine of the sender. Currently not usable.


#### returncode
int32

The return code of the transaction.
0  = ok
!0 = error, most probably a message number from uc.msl

#### durationTotal
double

The duration in seconds required to process the transaction.

#### durationDB
double

The time in seconds spent in DB calls.


#### beginTime
timestamp in ISO 8601 compatible format "2016-11-15 11:47:16.866"

The time when the transaction started.


#### lineFrom
uint64

The line number at which the transaction started in the original trace file.

#### endTime
timestamp in ISO 8601 compatible format "2016-11-15 11:47:16.866"

The time when the transaction was finished.

#### lineTo
uint64

The line number at which the transaction ended in the original trace file.
