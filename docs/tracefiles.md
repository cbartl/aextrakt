## tracefiles.csv

Contains general information about the tracefile and the process it traces.

One line per scanned file which contains a known trace format.

Description of all variables (columns):

#### filename
string

Only filename of the scanned file, no full path.

#### processType
string, factor

What kind of server process does it trace?
Supported types are WP, CP, JWP. (JCP coming soon).

#### version
string

Full version string of the executable. Sub-components (like DLLs) may have different version,
e.g. 12.0.0+hf.1.build.3538

#### buildDate
timestamp in ISO 8601 compatible format "2016-10-14 13:25:55"

Build date of the executable.  Sub-components (like DLLs) may have different build dates.

#### iniFile
string

Path of the ini-file used by the process

#### OdbcConnectionString
string

The connection string used by "old" server processes for database connection.

#### JdbcConnectionString
string

The connection string used by Java processes for database connection.

#### hostName
string

Hostname where the process runs. May be empty.

#### ipAddress
string

IP-Address of host where the process runs.

#### pid
int64 (why??? 😕)

Process Id in the operating system.

#### os
string

The operating system in some not very usable format, <br>
e.g. *Windows 6.1.7601 Service Pack 1 (2016-10-14 13:25:55 - W64a1)*

#### cpu
string

Empty. Not sure what the inventor intended with this variable.

#### cpuCount
int16

Number of CPUs or Cores, whatever the OS provides. Maybe used for licence stuff.

#### memory
int32

Available total memory of the host in MB

#### perfCpu
int64

Minimal performance measurement for CPU speed. Higher is better.

#### perfDb
int32

Minimal performance for DB or DB connection. Does some read/write operations in the DB.
Higher is better.

#### dbInfo
empty

New Years resoution to implement (no detailed spec. of year by intent 😇).
