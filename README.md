# aextrakt

aextrakt \[ɪkˈstrækt\] extracts data from Automation Engine (AE) traces.

## Usage

    aextrakt [OPTION]...
    aextrakt -i path-to-tracefiles -o output-path
    aextrakt --input=path-to-tracefiles --output=output-path

Optional arguments:

    -i, --input   Path of a folder which contains AE traces (default: .)
    -o, --output  Path of folder where all output is written to (default: out)

As all regular files in input directory are parsed, it should only
contain WP traces, nothing else. Folders are ok.


## Setup

I currently do not provide binaries and probably never will. You should not trust my binaries.
To compile yourself a modern C++ compiler is required. I always build and test with latest release of
GCC on Linux and MS Visual C++ on Windows.

A docker image or Dockerfile to ease the build process is planned. Please be patient.


## Output

The output files are
- Traces split into one file per transaction
- **[tracefiles.csv](docs/tracefiles.md)**: Data about the trace file and the process it traces.
- **[transactions.csv](docs/transactions.md)**: Data about all transactions of all processed trace files.
- **[events.csv](docs/events.md)**: Data about sent events, its sender and receiver (SEND and RCV).

### Kind of glossary

The Automation Engine documentation uses the word *message* for 2 different things:

1. The "MsgID" as used in RCV and SEND lines of trace.
1. A line in the log or trace file identified by a message number (U0000....)

In order to avoid confusion aextract always uses the term "event" for the MsgID from item 1.

- A transaction has an event-id.
- A log line has a particular message number.
- Usage of the term "*MsgID*" in aextract may be considered as bug.


## Contribute

Spread the word, ask questions, report bugs and success stories.


## Build

Build time dependencies:
- boost program_options
- gtest

Builds tested with latest GCC and Visual Studio Community Edition.

For Visual Studio you can install the dependencies with vcpkg

    vcpkg install boost:x64-windows gtest:x64-windows

