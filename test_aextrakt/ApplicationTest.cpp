/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "libaextrakt/Application.h"

#include <gtest/gtest.h>
#include <string>
#include <experimental/filesystem>


TEST(Application, validateArgs__DefaultCmdlineArgs) {
    namespace fs = std::experimental::filesystem;
    uc4::Application& app = uc4::Application::instance();

    int argc = 1;
    const char* argv[1] = { "aextrakt.exe" };

    app.validateArgs(argc, argv);

    fs::path dir{ "." };
    EXPECT_EQ(dir.string(), app.tracedir().string());

    dir /= "out";
    EXPECT_EQ(dir.string(), app.outdir().string());
}


TEST(Application, validateArgs__OnlyInputCmdlineArgs) {
    namespace fs = std::experimental::filesystem;
    uc4::Application& app = uc4::Application::instance();

    fs::path test_indir{ "." };
    test_indir /= ".";
    std::string sindir{ test_indir.string() };
    const char* szindir = sindir.c_str();

    int argc = 3;
    const char* argv[] = { "aextrakt.exe", "-i", const_cast<char*>(szindir) };

    app.validateArgs(argc, argv);

    EXPECT_EQ(test_indir.string(), app.tracedir());

    fs::path expected_outdir{ test_indir / "out" };
    EXPECT_EQ(expected_outdir.string(), app.outdir());
}

TEST(Application, validateArgs__OnlyOutputCmdlineArgs) {
    namespace fs = std::experimental::filesystem;
    uc4::Application& app = uc4::Application::instance();

    fs::path test_outdir{ "." };
    test_outdir /= ".";
    std::string soutdir{ test_outdir.string() };
    const char* szoutdir = soutdir.c_str();

    int argc = 3;
    const char* argv[] = { "aextrakt.exe", "-o", const_cast<char*>(szoutdir) };

    app.validateArgs(argc, argv);

    std::string expected_indir{ "." };
    EXPECT_EQ(expected_indir, app.tracedir());

    EXPECT_EQ(test_outdir.string(), app.outdir());
}

TEST(Application, validateArgs__ShortCmdlineArgs) {
    namespace fs = std::experimental::filesystem;
    uc4::Application& app = uc4::Application::instance();

    fs::path test_indir{ "." };
    test_indir /= ".";
    std::string sindir{ test_indir.string() };
    const char* szindir = sindir.c_str();

    fs::path test_outdir{ "." };
    test_outdir /= ".";
    std::string soutdir{ test_outdir.string() };
    const char* szoutdir = soutdir.c_str();

    int argc = 5;
    const char* argv[] = { "aextrakt.exe"
        , "-o", const_cast<char*>(szoutdir)
        , "-i", const_cast<char*>(szindir) };

    app.validateArgs(argc, argv);

    EXPECT_EQ(sindir, app.tracedir());

    EXPECT_EQ(soutdir, app.outdir());
}


// params "--input=.\." and "--output .\."
TEST(Application, validateArgs__LongCmdlineArgs) {
    namespace fs = std::experimental::filesystem;
    uc4::Application& app = uc4::Application::instance();

    fs::path test_indir{ "." };
    test_indir /= ".";
    std::string sindir{ test_indir.string() };
    std::string sinparam{ std::string{ "--input=" } +sindir };
    const char* szinparam = sinparam.c_str();

    fs::path test_outdir{ "." };
    test_outdir /= ".";
    std::string soutdir{ test_outdir.string() };
    const char* szoutdir = soutdir.c_str();

    int argc = 4;
    const char* argv[] = { "aextrakt.exe"
        , "--output", const_cast<char*>(szoutdir)
        , const_cast<char*>(szinparam) };

    app.validateArgs(argc, argv);

    EXPECT_EQ(sindir, app.tracedir());

    EXPECT_EQ(soutdir, app.outdir());
}
