/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "libaextrakt/FileInfo.h"
#include "libaextrakt/Line.h"
#include "libaextrakt/Transaction.h"
#include "libaextrakt/Parsers.h"

#include <gtest/gtest.h>
#include <regex>
#include <array>

static uc4::Ucmsl emptyUcmsl{ std::make_unique<std::istringstream>( "" ) };

TEST( Line__getMsgNr, positive )
{
    uc4::FileInfo fi{ emptyUcmsl };

    std::string line{
            "20161115/114655.939 - U0003400 Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started." };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( line );

    EXPECT_EQ( 3400, uc4::Line( line, fi ).getMsgNr() );
    EXPECT_EQ( -1, uc4::Line(
            "20161115/114655.939 - U000340 Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started.",
            fi ).getMsgNr() );
    EXPECT_EQ( -1, uc4::Line(
            "20161115/114655.939 - X0003400 Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started.",
            fi ).getMsgNr() );
}

TEST( Line__tryReadSendEvent, positive )
{
    uc4::FileInfo fi{ emptyUcmsl };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( "20161115/114718.796 - U00001234 xxx" );

    std::array<std::string, 5> s{
            "20161115/115247.182 -   UCGENX_R            SEND <uc-env  to  *CP001#00000007                           MQ1CP001 MsgID: 0000000156 b-acv: 00000000 c-acv: 00000000 MsgType: ? Prio: 200 ClientPrio: 200 Client: 0000 UserID: 0000000000",
            "20161115/115247.192 -   UCGENX_R            SEND EXEC     to  *SERVER                          JPEXEC_R MQWP     MsgID: 0000000722 b-acv: 00000000 c-acv: 00000000 MsgType: W Prio: 200 ClientPrio: 200 Client: 0100 UserID: 0001001003",
            "20161115/115247.470 -   UCDS_R              SEND <eh      to  *CP001#00000007 239                       MQ1CP001 MsgID: 0000000160 b-acv: 00000000 c-acv: 00000000 MsgType: ? Prio: 200 ClientPrio: 200 Client: 0000 UserID: 0000000000",
            "20161115/114718.965 -   JPEXEC_R            SEND <GEN     to  *SERVER         OX                        MQWP     MsgID: 0000000044 b-acv: 00000000 c-acv: 00000000 MsgType: W Prio: 212 ClientPrio: 200 Client: 0000 UserID: 0000000000",
            "20161115/114712.205 -   UCMAIN_R            SEND SVC_FND  to  SQL9_MY#CP001"
    };

    uc4::EventInfo e;

    EXPECT_EQ( true, uc4::Line( s[0], fi ).tryReadSendEvent( e ) );
    EXPECT_EQ( "UCGENX_R", e.senderSR );
    EXPECT_EQ( "<uc-env", e.name );
    EXPECT_EQ( "*CP001#00000007", e.receiverProcess );
    EXPECT_EQ( "", e.eventDetail );
    EXPECT_EQ( "", e.receiverSR );
    EXPECT_EQ( "MQ1CP001", e.queue );
    EXPECT_EQ( 156, e.eventId );
    EXPECT_EQ( "00000000", e.bacv );
    EXPECT_EQ( "00000000", e.cacv );
    EXPECT_EQ( '?', e.type );
    EXPECT_EQ( 200, e.prio );
    EXPECT_EQ( 200, e.clientPrio );
    EXPECT_EQ( 0, e.client );
    EXPECT_EQ( 0, e.userid );

    EXPECT_EQ( true, uc4::Line( s[1], fi ).tryReadSendEvent( e ) );
    EXPECT_EQ( "JPEXEC_R", e.receiverSR );
    EXPECT_EQ( 'W', e.type );

    EXPECT_EQ( true, uc4::Line( s[2], fi ).tryReadSendEvent( e ) );
    EXPECT_EQ( "239", e.eventDetail );

    EXPECT_EQ( true, uc4::Line( s[3], fi ).tryReadSendEvent( e ) );
    EXPECT_EQ( "OX", e.eventDetail );

    EXPECT_EQ( true, uc4::Line( s[4], fi ).tryReadSendEvent( e ) );
    EXPECT_EQ( "", e.eventDetail );
    EXPECT_EQ( "", e.queue );
    EXPECT_EQ( uc4::nan<long>(), e.eventId );
    EXPECT_EQ( uc4::nan<long>(), e.prio );
}

TEST( Line__tryReadTransactionBegin, positive )
{
    uc4::FileInfo fi{ emptyUcmsl };
    fi.msgnrDigits      = 0;
    fi.setMsgnrDigits( "20161115/114718.796 - U00001234 xxx" );

    std::array<std::string, 5> s{
            "20161115/114716.859 - UCVAREXR              RCV  TIMER    frm *SERVER                                   MQWP     MsgID: 0000000000 c-acv: 00000000",
            "20161115/114720.080 - UCMAIN_R              RCV  WP_START frm SQL9_MY#WP001                             MQWP     MsgID: 0000000012 c-acv: 00000000",
            "20161115/114720.579 - UCMAIN_R              RCV  CP_STOP  frm SQL9_MY#CP001                             MQ1CP001 MsgID: 0000000011 c-acv: 00000000",
            "20161116/153619.175 - UCREPRTR              RCV  WRITE    frm SQL9_MY#WP001                             MQWP     MsgID: 0000000000 c-acv: 00000000"
    };


    using ParserType = uc4::TransactionBeginParser;
    ParserType parser{ fi };
    const bool retParse = parser.tryParse( s[0] );
    ASSERT_TRUE( retParse );

    auto d = parser.data();
    ASSERT_NE( nullptr, d );
    EXPECT_EQ( "2016-11-15 11:47:16.859", d->timestamp );
    EXPECT_EQ( "UCVAREXR", d->SR );
    EXPECT_EQ( "TIMER", d->name );
    EXPECT_EQ( "*SERVER", d->senderProcess );
    EXPECT_EQ( "", d->senderDetail );
    EXPECT_EQ( "", d->xreq );
    EXPECT_EQ( "MQWP", d->queue );
    EXPECT_EQ( 0, d->eventId );
    EXPECT_EQ( "00000000", d->cacv );
}

TEST( Line__split, allThree )
{
    uc4::FileInfo fi{ emptyUcmsl };

    std::string lineFromTrace{
            "20161115/114655.939 - U0003400 Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started." };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( lineFromTrace );

    uc4::Line line{ lineFromTrace, fi };
    line.split();

    EXPECT_TRUE( line.timestamp.has_value() );
    EXPECT_TRUE( line.msgnr.has_value() );
    EXPECT_TRUE( line.text.has_value() );

    EXPECT_EQ( *line.timestamp, std::string("20161115/114655.939") );
    EXPECT_EQ( *line.msgnr, 3400 );
    EXPECT_EQ( *line.text, "Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started.");
}

TEST( Line__split, timestampText )
{
    uc4::FileInfo fi{ emptyUcmsl };

    std::string lineFromTrace{
            "20161115/114655.939 - Grummlmumpf Test" };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( lineFromTrace );

    uc4::Line line{ lineFromTrace, fi };
    line.split();

    EXPECT_TRUE( line.timestamp.has_value() );
    EXPECT_FALSE( line.msgnr.has_value() );
    EXPECT_TRUE( line.text.has_value() );

    EXPECT_EQ( *line.timestamp, std::string("20161115/114655.939") );
    EXPECT_EQ( *line.text, std::string("Grummlmumpf Test") );
}

TEST( Line__split, textOnly )
{
    uc4::FileInfo fi{ emptyUcmsl };

    std::string lineFromTrace{
            "                                00000000  F6E4FCD6 C4DCDF7B 7D5B5D00 00000000" };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( lineFromTrace );

    uc4::Line line{ lineFromTrace, fi };
    line.split();

    EXPECT_FALSE( line.timestamp.has_value() );
    EXPECT_FALSE( line.msgnr.has_value() );
    EXPECT_TRUE( line.text.has_value() );

    EXPECT_EQ( lineFromTrace, *line.text );
}