/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "libaextrakt/FilePos.h"

#include <sstream>
#include <gtest/gtest.h>

TEST( FilePos__nextLine, positive )
{
    std::stringstream file;
    file << "line 1\nline 2\nline 3\n";

    uc4::FilePos filePos{ &file, 0 };
    auto line = filePos.nextLine();
    EXPECT_TRUE( line.has_value() );
    EXPECT_EQ( std::string("line 1"), *line );

    line = filePos.nextLine();
    EXPECT_TRUE( line.has_value() );
    EXPECT_EQ( std::string("line 2"), *line );

    line = filePos.nextLine();
    EXPECT_TRUE( line.has_value() );
    EXPECT_EQ( std::string("line 3"), *line );

    line = filePos.nextLine();
    EXPECT_FALSE( line.has_value() );
}