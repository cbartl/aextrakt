/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "libaextrakt/Tools.h"

#include <string>
#include <vector>
#include <gtest/gtest.h>
#include <locale>

TEST(Tools_convertListEntries, positive) {
    using std::vector;
    using std::string;
    vector<string> vs{ "abc", "123", "0.567" };

    string s;
    int i;
    double d;

    ASSERT_EQ(3, uc4::convertListEntries(vs, s, i, d));
    EXPECT_EQ("abc", s);
    EXPECT_EQ(123, i);
    EXPECT_DOUBLE_EQ(0.567, d);
}


TEST(Tools_convertListEntries, listTooSmall) {
    using std::vector;
    using std::string;
    vector<string> vs{ "abc", "123", "0.567" };

    string s;
    int i;
    double d;
    int dummy;

    ASSERT_EQ(3, uc4::convertListEntries(vs, s, i, d, dummy));
}

TEST(Tools_convertListEntries, convertionError) {
    using std::vector;
    using std::string;
    vector<string> vs{ "abc", "123", "0.567" };

    int i;
    ASSERT_EQ(0, uc4::convertListEntries(vs, i, i, i));
}

TEST(local, global) {
    double d;
    std::istringstream ss1("0.1234");
    ss1 >> d;

    std::locale newLoc(std::locale(), new uc4::numpunct);
    std::locale oldLoc = std::locale::global(newLoc);

    std::istringstream ss2("0,5678");
    ss2 >> d;

    std::locale::global(oldLoc);

    std::istringstream ss3("0.1234");
    ss3 >> d;
}

TEST(stripCarriageReturn, with_cr) {
    std::string s{ "grumml\r" };
    uc4::stripCarriageReturn(s);
    EXPECT_EQ("grumml", s);
}
TEST(stripCarriageReturn, without_cr) {
    std::string s{ "grumml" };
    uc4::stripCarriageReturn(s);
    EXPECT_EQ("grumml", s);
}
TEST(stripCarriageReturn, empty_string) {
    std::string s{ "" };
    uc4::stripCarriageReturn(s);
    EXPECT_EQ("", s);
}


TEST(convertAutomicToISO8601, positive) {
    const std::string automicFormat{"20161116/153419.784"};
    const std::string expected{"2016-11-16 15:34:19.784"};
    const std::string result = uc4::formatAutomicTimestamp(automicFormat);
    EXPECT_EQ(expected, result);

}