/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <gtest/gtest.h>
#include <Ucmsl.h>

using std::regex_search;
using std::istream;
using std::unique_ptr;

TEST( Ucmsl_load, minimal )
{
    unique_ptr<istream> input( new std::istringstream( "00000000DC|\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();

    auto entries{ ucmsl.entries_[uc4::Language::German].entries };
    ASSERT_EQ( 1, entries.size() );

    const uc4::MslEntry& entry{ entries[0] };
    ASSERT_EQ( 0, entry.msgnr );
    ASSERT_EQ( uc4::Language::German, entry.language );
    ASSERT_EQ( uc4::Type::C, entry.type );
    ASSERT_STREQ( "|", entry.text.c_str() );
    ASSERT_EQ( 0, entry.msgInsertIdx.size() );

    std::string s{ "|" };
    std::smatch m;
    auto        search_ret = regex_search( s, m, entry.regex );
    ASSERT_TRUE( search_ret );
    ASSERT_EQ( 1, m.size() );
    ASSERT_EQ( s, m[0].str() );
}

TEST( Ucmsl_load, oneParam )
{
    unique_ptr<istream> input(
            new std::istringstream( "00000008EEUnexpected response to message type '&01'.\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();

    auto entries{ ucmsl.entries_[uc4::Language::English].entries };
    ASSERT_EQ( 1, entries.size() );

    const uc4::MslEntry& entry{ entries[0] };
    ASSERT_EQ( 8, entry.msgnr );
    ASSERT_EQ( uc4::Language::English, entry.language );
    ASSERT_EQ( uc4::Type::Error, entry.type );
    ASSERT_STREQ( "Unexpected response to message type '&01'.", entry.text.c_str() );
    ASSERT_EQ( 1, entry.msgInsertIdx.size() );
    ASSERT_EQ( 1, entry.msgInsertIdx[0] );

    std::string s{ "Unexpected response to message type 'MyMsgType'." };
    std::smatch m;
    auto        search_ret = regex_search( s, m, entry.regex );
    ASSERT_TRUE( search_ret );
    ASSERT_EQ( 2, m.size() );
    ASSERT_EQ( s, m[0].str() );
    ASSERT_STREQ( "MyMsgType", m[1].str().c_str() );

    auto[msgInserts, error] = entry.tryExtractMsgInserts( s );
    ASSERT_FALSE( error.has_value() );
    ASSERT_EQ( 1, msgInserts.size() );
    ASSERT_STREQ( "MyMsgType", msgInserts[0].c_str() );

    //msgInserts = ucmsl.tryExtractMsgInserts(8, s, uc4::Language::English);
}

TEST( Ucmsl_load, twoParamsReorder )
{
    unique_ptr<istream> input(
            new std::istringstream(
                    "00003315DEFehler bei '&02' Aufruf, der Client mit ID '&01' existiert nicht mehr\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();

    auto entries{ ucmsl.entries_[uc4::Language::German].entries };
    ASSERT_EQ( 1, entries.size() );

    const uc4::MslEntry& entry{ entries[0] };
    ASSERT_EQ( 3315, entry.msgnr );
    ASSERT_EQ( uc4::Language::German, entry.language );
    ASSERT_EQ( uc4::Type::Error, entry.type );
    ASSERT_STREQ( "Fehler bei '&02' Aufruf, der Client mit ID '&01' existiert nicht mehr", entry.text.c_str() );
    ASSERT_EQ( 2, entry.msgInsertIdx.size() );
    ASSERT_EQ( 2, entry.msgInsertIdx[0] );
    ASSERT_EQ( 1, entry.msgInsertIdx[1] );

    std::string s{ "Fehler bei 'mystuff' Aufruf, der Client mit ID '0100' existiert nicht mehr" };
    std::smatch m;
    auto        search_ret = regex_search( s, m, entry.regex );
    ASSERT_TRUE( search_ret );
    ASSERT_EQ( 3, m.size() );
    ASSERT_EQ( s, m[0].str() );
    ASSERT_STREQ( "mystuff", m[1].str().c_str() );
    ASSERT_STREQ( "0100", m[2].str().c_str() );

    auto[msgInserts, error] = entry.tryExtractMsgInserts( s );
    ASSERT_FALSE( error.has_value() );
    ASSERT_EQ( 2, msgInserts.size() );
    ASSERT_STREQ( "0100", msgInserts[0].c_str() );
    ASSERT_STREQ( "mystuff", msgInserts[1].c_str() );
}

TEST( Ucmsl_load, twoParamsHole )
{
    unique_ptr<istream> input(
            new std::istringstream( "00003412DIAgent '&01' hat sich angemeldet (Client-Verbindung='&04').\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();

    auto germanEntries{ ucmsl.entries_[uc4::Language::German].entries };
    ASSERT_EQ( 1, germanEntries.size() );

    const uc4::MslEntry& entry{ germanEntries[0] };
    ASSERT_EQ( 3412, entry.msgnr );
    ASSERT_EQ( uc4::Language::German, entry.language );
    ASSERT_EQ( uc4::Type::Info, entry.type );
    ASSERT_STREQ( "Agent '&01' hat sich angemeldet (Client-Verbindung='&04').", entry.text.c_str() );
    ASSERT_EQ( 4, entry.msgInsertIdx.size() );
    ASSERT_EQ( 1, entry.msgInsertIdx[0] );
    ASSERT_EQ( 4, entry.msgInsertIdx[1] );
    ASSERT_EQ( 0, entry.msgInsertIdx[2] );
    ASSERT_EQ( 0, entry.msgInsertIdx[3] );

    std::string s{ "Agent 'WIN01' hat sich angemeldet (Client-Verbindung='123456789')." };
    std::smatch m;
    auto        search_ret = regex_search( s, m, entry.regex );
    ASSERT_TRUE( search_ret );
    ASSERT_EQ( 3, m.size() );
    ASSERT_EQ( s, m[0].str() );
    ASSERT_STREQ( "WIN01", m[1].str().c_str() );
    ASSERT_STREQ( "123456789", m[2].str().c_str() );
}

TEST( MslEntry_tryExtractMsgInserts, ok )
{
    unique_ptr<istream> input(
            new std::istringstream( "00003412DIAgent '&01' hat sich angemeldet (Client-Verbindung='&02').\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();

    auto germanEntries{ ucmsl.entries_[uc4::Language::German].entries };
    const uc4::MslEntry& entry{ germanEntries[0] };
    auto[msgInserts, error] = entry.tryExtractMsgInserts(
            "Agent 'MY_AGENT' hat sich angemeldet (Client-Verbindung='12345')." );

    ASSERT_FALSE( error.has_value() );
    ASSERT_EQ( 2, msgInserts.size() );
    ASSERT_EQ( "MY_AGENT", msgInserts[0] );
    ASSERT_EQ( "12345", msgInserts[1] );
}

TEST( MslEntry_tryExtractMsgInserts, Error )
{
    unique_ptr<istream> input(
            new std::istringstream( "00003412DIAgent '&01' hat sich angemeldet (Client-Verbindung='&02').\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();

    auto germanEntries{ ucmsl.entries_[uc4::Language::German].entries };
    const uc4::MslEntry& entry{ germanEntries[0] };
    auto[msgInserts, error] = entry.tryExtractMsgInserts(
            "Agent 'MY_AGENT' hat sich angemeldet (Client-Verbindung='12345'). xxx" );

    ASSERT_TRUE( error.has_value() );
    msgInserts.size();
}

TEST( Ucmsl_tryExtractMsgInserts, ok )
{
    unique_ptr<istream> input(
            new std::istringstream( "00003412DIAgent '&01' hat sich angemeldet (Client-Verbindung='&02').\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();
    auto[msgInserts, error] = ucmsl.tryExtractMsgInserts( 3412,
                                                          "Agent 'MY_AGENT' hat sich angemeldet (Client-Verbindung='12345').",
                                                          uc4::Language::German );
    ASSERT_FALSE( error.has_value() );
    ASSERT_EQ( 2, msgInserts.size() );
    ASSERT_EQ( "MY_AGENT", msgInserts[0] );
    ASSERT_EQ( "12345", msgInserts[1] );
}

TEST( Ucmsl_tryExtractMsgInserts, Error )
{
    unique_ptr<istream> input(
            new std::istringstream( "00003412DIAgent '&01' hat sich angemeldet (Client-Verbindung='&02').\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();
    auto[msgInserts, error] = ucmsl.tryExtractMsgInserts( 3412,
                                                          "Agent 'MY_AGENT' hat sich angemeldet (Client-Verbindung='12345'). xxx",
                                                          uc4::Language::German );

    ASSERT_TRUE( error.has_value() );
    msgInserts.size();
}

TEST( Ucmsl_extractMsgInserts, ok )
{
    unique_ptr<istream> input(
            new std::istringstream( "00003412DIAgent '&01' hat sich angemeldet (Client-Verbindung='&02').\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();
    auto msgInserts = ucmsl.extractMsgInserts( 3412,
                                               "Agent 'MY_AGENT' hat sich angemeldet (Client-Verbindung='12345').",
                                               uc4::Language::German );
    ASSERT_EQ( 2, msgInserts.size() );
    ASSERT_EQ( "MY_AGENT", msgInserts[0] );
    ASSERT_EQ( "12345", msgInserts[1] );
}

TEST( Ucmsl_extractMsgInserts, Error )
{
    unique_ptr<istream> input(
            new std::istringstream( "00003412DIAgent '&01' hat sich angemeldet (Client-Verbindung='&02').\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();

    ASSERT_THROW( ucmsl.extractMsgInserts( 3412,
                                           "Agent 'MY_AGENT' hat sich angemeldet (Client-Verbindung='12345'). xxx",
                                           uc4::Language::German ), uc4::MsgnrForLanguageNotFound );
}

TEST( Ucmsl_extractMsgInserts_unknown_language, ok )
{
    unique_ptr<istream> input(
            new std::istringstream( "00003412DIAgent '&01' hat sich angemeldet (Client-Verbindung='&02').\n"
                                            "00003412EIAgent '&01' logged off (client connection='&02').\n"
                                            "00003412FIFrench Agent '&01' logged off (client connection='&02').\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();

    std::set<uc4::Language> languageCandidates;
    languageCandidates.insert(uc4::Language::German);
    languageCandidates.insert(uc4::Language::English);
    languageCandidates.insert(uc4::Language::French);

    std::optional<uc4::Language> language;
    auto msgInserts = ucmsl.extractMsgInserts( 3412,
                                               "Agent 'MY_AGENT' hat sich angemeldet (Client-Verbindung='12345').",
                                               language,
                                               languageCandidates );
    ASSERT_TRUE( language.has_value() );
    ASSERT_EQ( 1, languageCandidates.size() );
    ASSERT_EQ( 2, msgInserts.size() );
    ASSERT_EQ( "MY_AGENT", msgInserts[0] );
    ASSERT_EQ( "12345", msgInserts[1] );
}

TEST( Ucmsl_extractMsgInserts_unknown_language, error )
{
    unique_ptr<istream> input(
            new std::istringstream( "00003412DIAgent '&01' hat sich angemeldet (Client-Verbindung='&02').\n"
                                            "00003412EIAgent '&01' logged off (client connection='&02').\n"
                                            "00003412FIFrench Agent '&01' logged off (client connection='&02').\n" ) );
    uc4::Ucmsl          ucmsl( std::move( input ) );
    ucmsl.load();

    std::set<uc4::Language> languageCandidates;
    languageCandidates.insert(uc4::Language::German);
    languageCandidates.insert(uc4::Language::English);
    languageCandidates.insert(uc4::Language::French);

    std::optional<uc4::Language> language;
    auto msgInserts = ucmsl.extractMsgInserts( 3412,
                                               "Agent 'MY_AGENT' hat sich angemeldet (Client-Verbindung='12345'). xxx",
                                               language,
                                               languageCandidates );
    ASSERT_FALSE( language.has_value() );
    ASSERT_EQ( 0, languageCandidates.size() );
    ASSERT_EQ( 0, msgInserts.size() );
}
