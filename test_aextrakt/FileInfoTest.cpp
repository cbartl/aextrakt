/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "libaextrakt/FileInfo.h"
#include "Tools.h"

#include <sstream>
#include <gtest/gtest.h>

static uc4::Ucmsl emptyUcmsl{ std::make_unique<std::istringstream>( "" ) };


TEST( FileInfo, setMsgnrDigits__DefaultCmdlineArgs )
{
    uc4::FileInfo fi{ emptyUcmsl };

    // 8 digits
    fi.setMsgnrDigits(
            "20161115/114655.939 - U00003400 Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started." );
    EXPECT_EQ( 8, fi.msgnrDigits );

    // 7 digits
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits(
            "20161115/114655.939 - U0003400 Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started." );
    EXPECT_EQ( 7, fi.msgnrDigits );

    // no check when it's already initialized 
    fi.msgnrDigits = 1;
    fi.setMsgnrDigits(
            "20161115/114655.939 - U00003400 Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started." );
    EXPECT_EQ( 1, fi.msgnrDigits );

    // Line does not contain msgnr
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits(
            "20161115/114655.939 - Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started." );
    EXPECT_EQ( 0, fi.msgnrDigits );

    // Min line length
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( "20161115/114655.939 - U0003400" );
    EXPECT_EQ( 7, fi.msgnrDigits );

    // No Msg Nr.
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( "20161115/114655.939 - U0000340X" );
    EXPECT_EQ( 0, fi.msgnrDigits );

}

TEST( FileInfo, getFileInfo__WPtraceChange )
{
    std::istringstream s{
            "20161202/194442.411 - U00003450 The TRACE file was opened with the switches '2400000000000000'.\n"
                    "20161202/194442.411 - U00003380 Server 'UC4#WP001' version '11.2.3+build.416' (Runtime '13/16:52:09', Log# '34', Trc# '146').\n"
                    "20161202/194442.411 - U00003491 There is a time difference of '0/00:00:00' or '0' seconds to the Primary Server.\n"
                    "20161202/194442.411 - U00003379 The logging of the Server start is repeated in the following lines.\n"
                    "20161202/194442.411 - ================================================================================================================================\n"
    };
    uc4::FileInfo      fi{ makeUcmsl() };
    getFileInfo( s, fi );
    EXPECT_EQ( 8, fi.msgnrDigits );
    EXPECT_EQ( uc4::FileInfo::Trigger::TraceChange, fi.trigger );
    EXPECT_EQ( uc4::FileInfo::Process::WP, fi.processType );
}

TEST( FileInfo, getFileInfo__WPstart )
{
    std::istringstream s{
            "20161115/114655.939 - U00003400 Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started.\n"
                    "20161115/114655.940 - U00003327 Build Date: '2016-10-14', '13:25:55'\n"
                    "20161115/114655.940 - U00003433 Server was started with INI file 'C:\\_Automic\\__ini\\UCSrv.ini'.\n"
                    "20161115/114655.940 - ----------------------------------------------------------------------------------------------------\n"
    };
    uc4::FileInfo      fi{ makeUcmsl() };
    getFileInfo( s, fi );
    EXPECT_EQ( 8, fi.msgnrDigits );
    EXPECT_EQ( uc4::FileInfo::Trigger::ProcStart, fi.trigger );
    EXPECT_EQ( uc4::FileInfo::Process::WP, fi.processType );
}

TEST( FileInfo, getFileInfo__JWPstart )
{
    std::istringstream s{
            "20170117/011232.955 - U00003450 The TRACE file was opened with the switches '2300000000000000'.\n"
                    "20170117/011232.971 - U00003400 Server 'UC112L#WP' version '11.2.4+low.build.2780' (changelist '1480499389') started.\n"
                    "20170117/011232.971 - U00003433 Server was started with INI file 'E:\\UC4Systeme\\UC112L\\Server\\bin\\ucsrv2.ini'.\n"
                    "20170117/011232.971 - U02000090 Java Runtime Environment version: '1.8.0_101'\n"
                    "20170117/011232.971 - U02000091 Java Runtime Environment vendor: 'Oracle Corporation'\n"
                    "20170117/011232.971 - U00045067 Java installation directory: 'C:\\Program Files\\Java\\jre1.8.0_101'\n"
                    "20170117/011232.971 - U02000192 Operating system: 'Windows Server 2012 R2', version: '6.3'.\n"
                    "20170117/011232.971 - U02000193 JVM Architecture: 'amd64'\n"
                    "20170117/011232.971 - U02000110 Maximum Heap Memory: '455' MB\n"
                    "20170117/011232.971 -           ----------------------------------------------------------------------------------------------------\n"
    };
    uc4::FileInfo      fi{ makeUcmsl() };
    getFileInfo( s, fi );
    EXPECT_EQ( 8, fi.msgnrDigits );
    EXPECT_EQ( uc4::FileInfo::Trigger::ProcStart, fi.trigger );
    EXPECT_EQ( uc4::FileInfo::Process::JWP, fi.processType );
}

TEST( FileInfo, getFileInfo__JWPtraceChange )
{
    std::istringstream s2{
            "20170116/233740.899 - U00003479 Logging was changed.\n"
                    "20170116/233740.899 - U00003400 Server 'UC112L#WP002' version '11.2.4+low.build.2780' (changelist '1480499389') started.\n"
                    "20170116/233740.915 - U00003433 Server was started with INI file 'E:\\UC4Systeme\\UC112L\\Server\\bin\\ucsrv2.ini'.\n"
                    "20170116/233740.915 - U02000090 Java Runtime Environment version: '1.8.0_101'\n"
                    "20170116/233740.915 - U02000091 Java Runtime Environment vendor: 'Oracle Corporation'\n"
                    "20170116/233740.915 - U00045067 Java installation directory: 'C:\\Program Files\\Java\\jre1.8.0_101'\n"
                    "20170116/233740.915 - U02000192 Operating system: 'Windows Server 2012 R2', version: '6.3'.\n"
                    "20170116/233740.915 - U02000193 JVM Architecture: 'amd64'\n"
                    "20170116/233740.915 - U02000110 Maximum Heap Memory: '455' MB\n"
                    "20170116/233740.915 -           ----------------------------------------------------------------------------------------------------\n"
    };
    uc4::FileInfo      fi{ makeUcmsl() };
    getFileInfo( s2, fi );
    EXPECT_EQ( 8, fi.msgnrDigits );
    EXPECT_EQ( uc4::FileInfo::Trigger::TraceChange, fi.trigger );
    EXPECT_EQ( uc4::FileInfo::Process::JWP, fi.processType );
}
