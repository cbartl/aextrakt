/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "libaextrakt/Transaction.h"
#include "libaextrakt/Parsers.h"

#include <gtest/gtest.h>
#include <string>
#include <array>
#include <numeric>

static uc4::Ucmsl emptyUcmsl{ std::make_unique<std::istringstream>( "" ) };

TEST( TransactionBeginParser, tryParse )
{
    uc4::FileInfo fi{ emptyUcmsl };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( "20161115/114718.796 - U00001234 xxx" );

    //std::array<std::string, 5> s{
    //    "20161115/114716.859 - UCVAREXR              RCV  TIMER    frm *SERVER                                   MQWP     MsgID: 0000000012 c-acv: 00000666",
    //    "20161115/114720.080 - UCMAIN_R              RCV  WP_START frm SQL9_MY#WP001                             MQWP     MsgID: 0000000012 c-acv: 00000000",
    //    "20161115/114720.579 - UCMAIN_R              RCV  CP_STOP  frm SQL9_MY#CP001                             MQ1CP001 MsgID: 0000000011 c-acv: 00000000",
    //    "20161116/153619.175 - UCREPRTR              RCV  WRITE    frm SQL9_MY#WP001                             MQWP     MsgID: 0000000000 c-acv: 00000000"
    //};
    std::string s{
            "20161115/114716.859 - UCVAREXR              RCV  TIMER    frm *SERVER                                   MQWP     MsgID: 0000000012 c-acv: 00000666" };

    using ParserType = uc4::TransactionBeginParser;
    ParserType parser{ fi };
    ASSERT_TRUE( parser.tryParse( s ) );

    auto d{ parser.data() };
    ASSERT_NE( nullptr, d );
    EXPECT_EQ( "2016-11-15 11:47:16.859", d->timestamp );
    EXPECT_EQ( "UCVAREXR", d->SR );
    EXPECT_EQ( "TIMER", d->name );
    EXPECT_EQ( "*SERVER", d->senderProcess );
    EXPECT_EQ( "", d->senderDetail );
    EXPECT_EQ( "", d->xreq );
    EXPECT_EQ( "MQWP", d->queue );
    EXPECT_EQ( 12, d->eventId );
    EXPECT_EQ( "00000666", d->cacv );
}


TEST( TransactionBeginParser, UCDS_R )
{
    uc4::FileInfo fi{ emptyUcmsl };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( "20161115/114718.796 - U00001234 xxx" );

    std::string s{
            "20161202/193646.283 - UCDS_R                RCV  <oh      frm *CP001#02377672 7    syncldap             MQ1CP001 MsgID: 0007363742 c-acv: 00000000" };

    using ParserType = uc4::TransactionBeginParser;
    ParserType parser{ fi };
    ASSERT_TRUE( parser.tryParse( s ) );

    auto d{ parser.data() };
    ASSERT_NE( nullptr, d );
    EXPECT_EQ( "2016-12-02 19:36:46.283", d->timestamp );
    EXPECT_EQ( "UCDS_R", d->SR );
    EXPECT_EQ( "<oh", d->name );
    EXPECT_EQ( "*CP001#02377672", d->senderProcess );
    EXPECT_EQ( "7", d->senderDetail );
    EXPECT_EQ( "syncldap", d->xreq );
    EXPECT_EQ( "MQ1CP001", d->queue );
    EXPECT_EQ( 7363742, d->eventId );
    EXPECT_EQ( "00000000", d->cacv );
}


TEST( JWPTransactionBeginParser, positive )
{
    uc4::FileInfo fi{ emptyUcmsl };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( "20161115/114718.796 - U00001234 xxx" );
    fi.processType = uc4::FileInfo::Process::JWP;
    fi.trigger     = uc4::FileInfo::Trigger::TraceChange;

    std::string s{
            "20161202/194618.699 - LdapMessageHandler    RCV  JLDAP    frm *SERVER                          UCUSER_R MQWP     MsgID: 0002331156 c-acv: 151837030" };

    using ParserType = uc4::JWPTransactionBeginParser;
    ParserType parser{ fi };
    ASSERT_TRUE( parser.tryParse( s ) );

    auto d{ parser.data() };
    ASSERT_NE( nullptr, d );
    EXPECT_EQ( "2016-12-02 19:46:18.699", d->timestamp );
    EXPECT_EQ( "LdapMessageHandler", d->SR );
    EXPECT_EQ( "JLDAP", d->name );
    EXPECT_EQ( "*SERVER", d->senderProcess );
    EXPECT_EQ( "UCUSER_R", d->senderSR );
    EXPECT_EQ( "MQWP", d->queue );
    EXPECT_EQ( 2331156, d->eventId );
    EXPECT_EQ( "090cd966", d->cacv );
}


TEST( SendEventParser, tryParse_longVariant )
{
    uc4::FileInfo fi{ emptyUcmsl };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( "20161115/114718.796 - U00001234 xxx" );

    std::string          s{
            "20161115/115247.192 -   UCGENX_R            SEND EXEC     to  *SERVER                          JPEXEC_R MQWP     "
                    "MsgID: 0000000722 b-acv: 00000123 c-acv: 00000456 MsgType: W Prio: 200 ClientPrio: 200 Client: 0100 UserID: 0001001003" };
    uc4::SendEventParser parser{ fi };
    ASSERT_TRUE( parser.tryParse( s ) );

    auto d{ parser.data() };
    ASSERT_NE( nullptr, d );

    EXPECT_EQ( "2016-11-15 11:52:47.192", d->timestamp );
    EXPECT_EQ( "UCGENX_R", d->senderSR );
    EXPECT_EQ( "EXEC", d->name );
    EXPECT_EQ( "*SERVER", d->receiverProcess );
    EXPECT_EQ( "", d->eventDetail );
    EXPECT_EQ( "JPEXEC_R", d->receiverSR );
    EXPECT_EQ( "MQWP", d->queue );
    EXPECT_EQ( 722, d->eventId );
    EXPECT_EQ( "00000123", d->bacv );
    EXPECT_EQ( "00000456", d->cacv );
    EXPECT_EQ( "W", d->type );
    EXPECT_EQ( 200, d->prio );
    EXPECT_EQ( 200, d->clientPrio );
    EXPECT_EQ( 100, d->client );
    EXPECT_EQ( 1001003, d->userid );
}


TEST( SendEventParser, tryParse_shortVariant )
{
    uc4::FileInfo fi{ emptyUcmsl };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( "20161115/114718.796 - U00001234 xxx" );

    std::string          s{ "20161115/115247.192 -   UCGENX_R            SEND EXEC     to  *SERVER" };
    uc4::SendEventParser parser{ fi };
    ASSERT_TRUE( parser.tryParse( s ) );

    auto d{ parser.data() };
    ASSERT_NE( nullptr, d );


    EXPECT_EQ( "2016-11-15 11:52:47.192", d->timestamp );
    EXPECT_EQ( "UCGENX_R", d->senderSR );
    EXPECT_EQ( "EXEC", d->name );
    EXPECT_EQ( "*SERVER", d->receiverProcess );
}


TEST( TransactionEndParser, tryParse )
{
    uc4::FileInfo fi{ emptyUcmsl };
    fi.msgnrDigits = 0;
    fi.setMsgnrDigits( "20161115/114718.796 - U00001234 xxx" );

    std::string s{ "20161115/114716.866 - EXIT UCVAREXR         RET: 0000000666 TIME: 0000,007 DB: 0000,003" };

    uc4::TransactionEndParser parser{ fi };
    ASSERT_TRUE( parser.tryParse( s ) );

    auto d{ parser.data() };
    ASSERT_NE( nullptr, d );

    EXPECT_EQ( "2016-11-15 11:47:16.866", d->timestamp );
    EXPECT_EQ( "UCVAREXR", d->SR );
    EXPECT_EQ( 666, d->ret );
    EXPECT_DOUBLE_EQ( 0000.007, d->timeTotal );
    EXPECT_DOUBLE_EQ( 0000.003, d->timeDB );
}

