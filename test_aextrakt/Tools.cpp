/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Tools.h"

uc4::Ucmsl&
makeUcmsl()
{
    static uc4::Ucmsl msl{
            std::make_unique<std::istringstream>(
                    "00003327DIErstellungsdatum: '&01', '&02'\n"
                            "00003327EIBuild Date: '&01', '&02'\n"
                            "00003327FIDate de création: '&01', '&02'\n"
                            "00003379DIIn den folgenden Zeilen wird das Logging des Serverstarts wiederholt ausgegeben.\n"
                            "00003379EIThe logging of the Server start is repeated in the following lines.\n"
                            "00003379FILa trace de démarrage du serveur est répétée dans les lignes suivantes.\n"
                            "00003380DIServer '&01' Version '&02' (Laufzeit '&03', Log# '&04', Trc# '&05')\n"
                            "00003380EIServer '&01' version '&02' (Runtime '&03', Log# '&04', Trc# '&05').\n"
                            "00003380FIServeur '&01' version '&02' (runtime '&03', log '&04', trace '&05').\n"
                            "00003400DIServer '&01' Version '&02' (Changelist '&03') gestartet.\n"
                            "00003400EIServer '&01' version '&02' (changelist '&03') started.\n"
                            "00003400FIServeur '&01' version '&02' (changelist '&03') activé.\n"
                            "00003433DIDer Server wurde mit der INI-Datei '&01' gestartet.\n"
                            "00003433EIServer was started with INI file '&01'.\n"
                            "00003433FILe serveur a été démarré avec le fichier INI '&01'.\n"
                            "00003450DIDie TRACE-Datei wurde mit den Schaltern '&01' geöffnet.\n"
                            "00003450EIThe TRACE file was opened with the switches '&01'.\n"
                            "00003450FIOuverture du fichier TRACE avec les paramètres '&01'.\n"
                            "00003491DIZeitdifferenz '&01' bzw. '&02' Sekunden zum Primärserver\n"
                            "00003491EIThere is a time difference of '&01' or '&02' seconds to the Primary Server.\n"
                            "00003491FIDifférence d'horloge de '&01' ou '&02' secondes avec le serveur primaire.\n"
                            "00045067DIJava Installationsverzeichnis: '&01'\n"
                            "00045067EIJava installation directory: '&01'\n"
                            "00045067FIRépertoire d'installation Java : '&01'\n"
                            "02000090DIVersion der Java Laufzeitumgebung: '&01'\n"
                            "02000090EIJava Runtime Environment version: '&01'\n"
                            "02000090FIEnvironnement runtime Java : version '&01'\n"
                            "02000091DIHersteller der Java Laufzeitumgebung: '&01'\n"
                            "02000091EIJava Runtime Environment vendor: '&01'\n"
                            "02000091FIEnvironnement runtime Java : vendeur '&01'\n"
                            "02000192DIBetriebssystem: '&01', Version: '&02'.\n"
                            "02000192EIOperating system: '&01', version: '&02'.\n"
                            "02000192FISystème d'exploitation : '&01', Version : '&02'.\n"
                            "02000193DIJVM Architektur: '&01'\n"
                            "02000193EIJVM Architecture: '&01'\n"
                            "02000193FIArchitecture JVM : '&01'\n"
                            "02000110DIMaximaler Speicher: '&01' MB\n"
                            "02000110EIMaximum Heap Memory: '&01' MB\n"
                            "02000110FIMémoire maximum : '&01' MB\n"
            ) };
    static bool isLoaded = false;
    if (!isLoaded)
    {
        msl.load();
        isLoaded = true;
    }
    return msl;
}
