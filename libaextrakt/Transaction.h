/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include "Parsers.h"
#include "Tools.h"

#include <string>
#include <regex>
#include <iostream>
#include <sstream>


namespace uc4 {

    using std::string;

    class Transaction
    {
    public:
        string filename = "";
        string line = "";
        string timeFrom = "";
        string timeTo = "";
        string senderProcess = "";
        string senderDetail;
        string xreq;
        int32_t returncode{ uc4::nan<int32_t>() };
        double durationTotal{ uc4::nan<double>() };
        double durationDB{ uc4::nan<double>() };
        size_t lineFrom = 0;
        size_t lineTo = 0;
        string hexMessage = "";

        //EventInfo
        string name = "";
        string senderSR = "";
        string SR = "";
        string queue = "";
        int32_t eventId{ uc4::nan<int32_t>() };
        string cacv;


        void dump(size_t lvl);
        Transaction() {}

        Transaction(const TransactionBeginParser::Data& d)
            : timeFrom{ d.timestamp }, senderProcess{ d.senderProcess }, senderDetail{ d.senderDetail },
              xreq{ d.xreq }, name{ d.name },  SR{ d.SR }, queue{ d.queue }, eventId{ d.eventId }, cacv{ d.cacv }
        {}

        void setTransactionEnd(const TransactionEndParser::Data& d, const size_t lineno) {
            timeTo = d.timestamp;
            returncode = d.ret;
            durationTotal = d.timeTotal;
            durationDB = d.timeDB;
            lineTo = lineno;
        }

        static void printCSVheader(std::ostream& os);
        void printCSV(std::ostream& os = std::cout);

        static void printSendEventCSVheader(std::ostream& os);
        void printSendEventCSV(std::ostream & os, const SendEventParser::Data & d);


    };


}
