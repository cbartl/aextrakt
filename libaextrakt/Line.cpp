/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <algorithm>
#include <cstdint>
#include <boost/algorithm/string.hpp>

#include "Line.h"
#include "Parsers.h"


namespace uc4 {

    using std::string;




    bool Line::tryReadSendEvent(EventInfo& e) {
        if (_line.length() < 64) return false;
        if (getMsgNr() != uc4::nan<long>()) return false; // Send has no MsgNr
        if (_line.substr(43, 6) != " SEND ") return false;
        if (_line.substr(57, 4) != " to ") return false;

        e.clear();
        e.senderSR = boost::trim_copy(_line.substr(22, 21));
        e.name = boost::trim_right_copy(_line.substr(49, 8));
        e.receiverProcess = boost::trim_right_copy(_line.substr(62, 15));

        if (_line.length() < 231)
            return true;

        e.eventDetail = boost::trim_copy(_line.substr(78, 16));
        e.receiverSR= boost::trim_right_copy(_line.substr(95, 8));
        e.queue = boost::trim_right_copy(_line.substr(104, 8));

        e.eventId = std::stol(_line.substr(120, 10));
        e.bacv = _line.substr(138, 8);
        e.cacv = _line.substr(154, 8);
        e.type = _line[172];
        e.prio = std::stol(_line.substr(180, 3));
        e.clientPrio = std::stol(_line.substr(196, 3));
        e.client = std::stol(_line.substr(208, 4));
        e.userid = std::stol(_line.substr(221, 10));
        return true;
    }

}