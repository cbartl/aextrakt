/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include <string>
#include <vector>
#include <memory>
#include <array>

#include <boost/algorithm/string.hpp>
#include <boost/utility/string_view.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>

#include "ParseFunctions.h"
#include "Tools.h"
#include "FileInfo.h"


namespace uc4 {

    using std::string;

    class LineParser {
    public:
        const FileInfo& _fi;
        std::vector<string> _parts;
        int _msgNr{ uc4::undefined };

        LineParser(const FileInfo& fi) : _fi(fi) {}
        virtual ~LineParser() {}
        virtual bool tryParse(const string&) = 0;

        long getMsgNr(const string& line);

        const decltype(_parts)& parts() {
            return _parts;
        }
    };

    /********************************************************************************/ 
    // Transaction Begin

    class TransactionBeginParser : public LineParser {
    public:
        struct Data {
            string timestamp;
            string SR;
            string name;
            string senderProcess;
            string senderDetail;
            string xreq;
            string queue;
            int32_t eventId{ uc4::nan<long>() };
            string cacv;
        };
        TransactionBeginParser(const FileInfo& fi) : LineParser(fi) {}
        virtual ~TransactionBeginParser() {}
        enum Part : size_t {
            timestamp, SR, name, senderProcess, senderDetail, xreq, queue, eventId, cacv, COUNT
        };
        virtual bool tryParse(const string& line) {
            _parts.clear();

            if (line.length() < 146) return false;
            if (getMsgNr(line) != uc4::nan<long>()) return false;
            if (line.substr(43, 5) != " RCV ") return false;
            if (line.substr(57, 5) != " frm ") return false;

            _parts.resize(Part::COUNT);
            
            _parts[Part::timestamp] = formatAutomicTimestamp(line.substr(0, 19));
            _parts[Part::SR] = boost::trim_copy(line.substr(22, 21));
            _parts[Part::name] = boost::trim_right_copy(line.substr(49, 8));
            _parts[Part::senderProcess] = boost::trim_right_copy(line.substr(62, 15));
            _parts[Part::senderDetail] = boost::trim_right_copy(line.substr(78, 4)); // UCDS_R
            _parts[Part::xreq] = boost::trim_right_copy(line.substr(83, 20)); // UCDS_R
            _parts[Part::queue] = boost::trim_right_copy(line.substr(104, 8));
            _parts[Part::eventId] = line.substr(120, 10);
            _parts[Part::cacv] = line.substr(138, 8);

            return true;
        }
        std::unique_ptr <Data> data() {
            if (_parts.empty()) return nullptr;
            std::unique_ptr <Data> d{ new Data };
            const size_t retConvert = convertListEntries(_parts, d->timestamp, d->SR, d->name, d->senderProcess,
                                                         d->senderDetail, d->xreq, d->queue, d->eventId, d->cacv);
            if (retConvert != Part::COUNT)
                return nullptr;
            return d;
        }
    };



    class JWPTransactionBeginParser : public LineParser {
    public:
        struct Data {
            string timestamp;
            string SR;
            string name;
            string senderProcess;
            string senderSR;
            string queue;
            int32_t eventId{ uc4::nan<long>() };
            string cacv;
        };
        JWPTransactionBeginParser(const FileInfo& fi) : LineParser(fi) {}
        virtual ~JWPTransactionBeginParser() {}
        enum Part : size_t {
            timestamp, SR, name, senderProcess, senderSR, queue, eventId, cacv, COUNT
        };
        virtual bool tryParse(const string& line) {
            _parts.clear();

            if (line.length() < 146) return false;
            if (getMsgNr(line) != uc4::nan<long>()) return false;

            boost::char_separator<char> sep(" ");
            boost::tokenizer<decltype(sep)> tokens(line, sep);


            const std::array<boost::string_view, 13> checks{"", "-", "", "RCV", "", "frm", "", "", "", "MsgID:", "", "c-acv:", "" };

            size_t idx = 0;
            for (auto token : tokens) {
                // senderSR can be missing. In this case incr. idx
                if (idx == 7 && token.substr(0, 2) == "MQ")
                    idx++;

                // too many tokens
                if (idx >= checks.size())
                    return false;

                if (!checks[idx].empty() && token != checks[idx])
                        return false;
                

                _parts.resize(Part::COUNT);
                Part part = Part::COUNT;
                switch (idx) {
                case 0: part = Part::timestamp;
                        token = formatAutomicTimestamp(token);
                        break;
                case 2: part = Part::SR; break;
                case 4: part = Part::name; break;
                case 6: part = Part::senderProcess; break;
                case 7: part = Part::senderSR; break;
                case 8: part = Part::queue; break;
                case 10: part = Part::eventId; break;
                case 12: part = Part::cacv; break;
                }
                if (part != Part::COUNT)
                    _parts[part] = token;
                idx++;
            }
            return true;
        }
        std::unique_ptr <Data> data() {
            if (_parts.empty()) return nullptr;
            std::unique_ptr <Data> d{ new Data };
            std::int32_t cacv_;

            const size_t retConvert = convertListEntries(_parts, d->timestamp, d->SR, d->name, d->senderProcess,
                d->senderSR, d->queue, d->eventId, cacv_);
            if (retConvert != Part::COUNT)
                return nullptr;

            // Bug in JWP: c-acv is not hex-formatted.
            d->cacv = toHex(cacv_);

            return d;
        }
    };


    /********************************************************************************/
    // Transaction End

    class TransactionEndParser : public LineParser {
    public:
        struct Data {
            string timestamp;
            string SR;
            int32_t ret;
            double timeTotal;
            double timeDB;
        };
        TransactionEndParser(const FileInfo& fi) : LineParser(fi) {}
        virtual ~TransactionEndParser() = default;
        enum Part : size_t {
            timestamp, SR, ret, timeTotal, timeDB, COUNT
        };
        virtual bool tryParse(const string& line) {
            _parts.clear();
            if (line.length() != 87) return false;
            if (line.substr(21, 6) != " EXIT ") return false;

            _parts.resize(Part::COUNT);
            _parts[Part::timestamp] = formatAutomicTimestamp(line.substr(0, 19));
            _parts[Part::SR] = boost::trim_right_copy(line.substr(27, 8));
            _parts[Part::ret] = line.substr(49, 10);
            _parts[Part::timeTotal] = line.substr(66, 8);
            _parts[Part::timeDB] = line.substr(79, 8);
            _parts[Part::timeTotal][4] = _parts[Part::timeDB][4] = '.';
            return true;
        }

        std::unique_ptr <Data> data() {
            if (_parts.empty()) return nullptr;
            std::unique_ptr <Data> d{ new Data };


            const size_t retConvert = convertListEntries(_parts, d->timestamp, d->SR, d->ret, d->timeTotal, d->timeDB);
            if (retConvert != Part::COUNT)
                return nullptr;
            return d;
        }

    };

    class SendEventParser : public LineParser {
    public:
        struct Data {
            string timestamp;
            string senderSR;
            string name;
            string receiverProcess;
            string eventDetail;
            string receiverSR;
            string queue;
            int32_t eventId;
            string bacv;
            string cacv;
            string type;
            int16_t prio;
            int16_t clientPrio;
            int16_t client;
            int32_t userid;
        };
        SendEventParser(const FileInfo& fi) : LineParser(fi) {}
        virtual ~SendEventParser() = default;
        enum Part : size_t {
            timestamp, senderSR, name, receiverProcess, eventDetail, receiverSR, queue, eventId, bacv, cacv, type, prio, clientPrio, client, userid, COUNT
        };
        virtual bool tryParse(const string& line) {
            _parts.clear();
            if (line.length() < 64) return false;
            if (getMsgNr(line) != uc4::nan<long>()) return false; // Send has no MsgNr
            if (line.substr(43, 6) != " SEND ") return false;
            if (line.substr(57, 4) != " to ") return false;

            _parts.resize(Part::COUNT);

            _parts[Part::timestamp] = formatAutomicTimestamp(line.substr(0, 19));
            _parts[Part::senderSR] = boost::trim_copy(line.substr(22, 21));
            _parts[Part::name] = boost::trim_right_copy(line.substr(49, 8));
            _parts[Part::receiverProcess] = boost::trim_right_copy(line.substr(62, 15));

            if (line.length() < 231) {
                _parts.resize(4);
                return true;
            }

            _parts[Part::eventDetail] = boost::trim_copy(line.substr(78, 16));
            _parts[Part::receiverSR] = boost::trim_right_copy(line.substr(95, 8));
            _parts[Part::queue] = boost::trim_right_copy(line.substr(104, 8));
            _parts[Part::eventId] = line.substr(120, 10);
            _parts[Part::bacv] = line.substr(138, 8);
            _parts[Part::cacv] = line.substr(154, 8);
            _parts[Part::type] = line[172];
            _parts[Part::prio] = line.substr(180, 3);
            _parts[Part::clientPrio] = line.substr(196, 3);
            _parts[Part::client] = line.substr(208, 4);
            _parts[Part::userid] = line.substr(221, 10);
            return true;
        }

        std::unique_ptr <Data> data() {
            if (_parts.empty()) return nullptr;
            std::unique_ptr <Data> d{ new Data };

            const size_t retConvert = convertListEntries(_parts, 
                                d->timestamp, d->senderSR, d->name, d->receiverProcess, d->eventDetail, d->receiverSR,
                                d->queue, d->eventId, d->bacv, d->cacv, d->type, d->prio, d->clientPrio, d->client, d->userid);
            if (retConvert != Part::COUNT && retConvert != 4)
                return nullptr;
            return d;
        }

    };



    template <typename... parameters_t>
    bool convertParts(const LineParser& parser, parameters_t&... parameters) {
        return convertListEntries(parser._parts, parameters...);
    }

    long getMsgNr(const FileInfo& fi, const string& line);


}
