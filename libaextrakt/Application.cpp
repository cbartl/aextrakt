/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Application.h"
#include "File.h"

#include "Transaction.h"
#include "Ucmsl.h"

#include <fstream>
#include <stdexcept>

extern uc4::Application app;


namespace uc4
{

    void
    printHelp( po::options_description& optDescription )
    {
        std::cout << optDescription << std::endl;
    }

    void
    printVersion()
    {
        std::cout <<
                  "aextrakt 1, extract data from Automation Engine traces.\n\n"
                          "Copyright 2017 Christian Bartl.\n"
                          "License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.\n"
                          "This is free software: you are free to change and redistribute it.\n"
                          "There is NO WARRANTY, to the extent permitted by law.\n"
                          "\n"
                          "Written by Christian Bartl.\n";
    }


    int
    Application::validateArgs( int argc, const char** argv )
    {
        po::options_description optDescription{ "Allowed options" };
        po::variables_map       optVariables;
        optDescription.add_options()
                              ( "help,h", "Display this help text and exit" )
                              ( "version,V", "Display version information and exit" )
                              ( "input,i", po::value<decltype( _tracedir )>(),
                                "Folder which contains the WP traces (Default=.)" )
                              ( "output,o", po::value<decltype( _outdir )>(),
                                "Folder where to store the output(Default=./out)" );

        po::store( po::parse_command_line( argc, argv, optDescription ), optVariables );
        po::notify( optVariables );

        _tracedir = optVariables.count( "input" )
                    ? optVariables["input"].as<decltype( _tracedir )>()
                    : ".";
        _outdir   = optVariables.count( "output" )
                    ? optVariables["output"].as<decltype( _outdir )>()
                    : _tracedir / "out";

        if ( !fs::is_directory( tracedir() ) )
        {
            printHelp( optDescription );
            throw std::runtime_error( "Option \"input\" is not a directory" );
        }
        if ( fs::exists( outdir() ) && !fs::is_directory( outdir() ) )
        {
            printHelp( optDescription );
            throw std::runtime_error( "Option \"output\" is not a directory" );
        }

        if ( optVariables.count( "help" ) )
        {
            printHelp( optDescription );
            return 1;
        }

        if ( optVariables.count( "version" ) )
        {
            printVersion();
            return 1;
        }

        return 0;
    }


    int
    Application::run( int argc, const char** argv )
    {
        int ret = 0;
        try
        {
            if ( validateArgs( argc, argv ) )
                return ret;

            // create output folder
            if ( !fs::exists( outdir() ) )
                fs::create_directories( outdir() );
            // TODO delete out/0.txt: all others get overwritten. In 0.txt we always append.

            // create out/transactions.csv
            std::ofstream transactionsFile( outdir() / "transactions.csv" );
            std::ofstream messagesFile( outdir() / "events.csv" );
            std::ofstream fileInfoFile( outdir() / "tracefiles.csv" );
            Transaction::printCSVheader( transactionsFile );
            Transaction::printSendEventCSVheader( messagesFile );
            File::printCSVheader( fileInfoFile );

            // initialize uc.msl
            Ucmsl ucmsl{ fs::current_path(),
                         fs::path{ argv[0] }.parent_path(),
                         tracedir() };
            ucmsl.load();


            for ( auto& de : fs::directory_iterator( tracedir() ) )
            {
                if ( de.status().type() != fs::file_type::regular )
                    continue;

                FileInfo fi{ ucmsl };

                const fs::path& pth{ de };
                std::cout << "Processing " << pth.filename() << std::endl;
                fi.fileName = pth.filename().string();
                try
                {
                    File f( pth, transactionsFile, messagesFile, fileInfoFile,
                            ucmsl );
                    f.run();
                }
                catch ( File::TypeUnsupported& e )
                {
                    std::cerr << "    " << e.what() << std::endl;
                }
            }

            std::cout << "<<< end >>>" << std::endl;
        }
        catch ( const std::exception& e )
        {
            std::cerr << "\nError: " << e.what() << std::endl;
            ret = 1;
        }

        return ret;
    }

}
