/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <string>
#include <locale>
#include <boost/utility/string_view.hpp>

#include "Parsers.h"

namespace uc4 {

    long LineParser::getMsgNr(const std::string& line) {
        if (_msgNr != undefined)
            return _msgNr;
        if (_fi.msgnrDigits == 0)
            return nan<long>();

        return uc4::getMsgNr(_fi, line);

    }

    
    long getMsgNr(const FileInfo& fi, const std::string& line) {
        return getMsgNr(line, fi.msgnrDigits);
    }
}