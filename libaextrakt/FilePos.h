/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef AEXTRAKT_FILEPOS_H
#define AEXTRAKT_FILEPOS_H


#include "Tools.h"
#include <istream>
#include <string>
#include <optional>
#include <memory>


namespace uc4
{

    using std::istream;
    using std::string;
    using std::optional;
    using std::shared_ptr;

    class FilePos
    {
    public:
        istream* file = nullptr;
        size_t lineNo = 0;

        optional<string>
        nextLine()
        {
            optional<string> ret;
            if ( string s;
            getlineStripCR( *file, s ) ) {
                ret = s;
                ++lineNo;
            }
            return ret;
        }
    };

}
#endif //AEXTRAKT_FILEPOS_H
