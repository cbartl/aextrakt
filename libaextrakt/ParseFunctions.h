/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include <algorithm>
#include <string>
#include <optional>
#include <locale>
#include <cctype>

#include <boost/utility/string_view.hpp>
#include "Tools.h"

namespace uc4
{

    constexpr size_t contentBegin    = 22;
    constexpr size_t timestampLength = 19;

    inline size_t
    getMsgNrDigits( const std::string& line )
    {
        if ( line.length() < contentBegin + 8 ) return nan<size_t>();   // Line too short, cannot contain a msg number

        std::istringstream ss( line );
        ss.seekg( std::streamoff( contentBegin ), ss.beg );

        char c;
        ss >> c;
        if ( c != 'U' ) return nan<size_t>();       // msg number candidate must begin with 'U'

        std::string s;
        ss >> s;
        if ( s.length() < 7 || s.length() > 8 ) return nan<size_t>();   // The word excl. 'U' must be 7-8 characters ...

        size_t digits = static_cast <size_t> (std::count_if( s.begin(), s.end(), ::isdigit ));
        if ( digits != s.length() ) return nan<size_t>();
        return digits;
    }

    inline long
    getMsgNr( const std::string& line, const size_t msgnrDigits = nan<size_t>() )
    {
        const long   nanret = nan<long>();
        const size_t digits = (msgnrDigits == nan<size_t>() ? getMsgNrDigits( line ) : msgnrDigits);
        if ( digits == nan<size_t>() ) return nanret;
        if ( line.length() < contentBegin + msgnrDigits ) return nanret;
        if ( line[contentBegin] != 'U' ) return nanret;
        boost::string_view smsg{ line.data() + contentBegin + 1, msgnrDigits };
        std::locale        loc;
        for ( auto         c : smsg )
        {
            if ( !std::isdigit( c, loc ) )
                return nanret;
        }
        return std::stol( smsg.to_string() );
    }


    using std::tuple;
    using std::optional;
    using boost::string_view;
    using std::string;
    using std::get;

    inline
    optional<string_view>
    getTimestamp( const string& line )
    {
        optional < string_view > ret;
        if ( line.length() < timestampLength )
            return ret;
        string_view tscand{ line };
        tscand.remove_suffix( tscand.size() - timestampLength );

        if ( tscand[8] == '/'
             && tscand[15] == '.'
             && std::isdigit( tscand[0] )
             && std::isdigit( tscand[1] )
             && std::isdigit( tscand[2] )
             && std::isdigit( tscand[3] )
             && std::isdigit( tscand[4] )
             && std::isdigit( tscand[5] )
             && std::isdigit( tscand[6] )
             && std::isdigit( tscand[7] )
             && std::isdigit( tscand[9] )
             && std::isdigit( tscand[10] )
             && std::isdigit( tscand[11] )
             && std::isdigit( tscand[12] )
             && std::isdigit( tscand[13] )
             && std::isdigit( tscand[14] )
             && std::isdigit( tscand[16] )
             && std::isdigit( tscand[17] )
             && std::isdigit( tscand[18] )
                )
        {
            ret = tscand;
        }
        return ret;
    }

    inline
    optional<string_view>
    getText( const string& line, const size_t msgnrDigits = nan<size_t>() )
    {
        //ts U666 abc
        optional < string_view > ret;
        if ( line.length() < contentBegin + sizeof( 'U' ) + msgnrDigits + sizeof( ' ' ) + sizeof( char ) )
            return ret;
        string_view sv{ line };
        sv.remove_prefix( std::min( sv.size(), contentBegin + msgnrDigits + 2 ) );
        if ( !sv.empty() )
            ret = sv;

        return ret;
    }

    inline
    tuple<optional<string_view>, optional<long>, optional<string_view>>
    splitLine( const string& line, const size_t msgnrDigits = nan<size_t>() )
    {
        tuple<optional<string_view>, optional<long>, optional<string_view>> ret;
        get<2>( ret ) = line;

        get<0>( ret ) = getTimestamp( line );
        if ( !get<0>( ret ) )
            return ret;

        get<2>( ret )->remove_prefix( get<0>(ret)->length() + 3 );

        if ( int msgnr = getMsgNr( line, msgnrDigits );
        msgnr != nan<long>() )
        get<1>( ret ) = msgnr;

        if ( !get<1>( ret ) )
            return ret;

        get<2>( ret ) = getText( line, msgnrDigits );

        return ret;
    }

}

