/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Tools.h"

namespace uc4 {

    void stripCarriageReturn(std::string& s) {
        const size_t size = s.size();
        if (size == 0) return;

        const size_t pos = size - 1;
        if (s[pos] == '\r')
            s.resize(pos);
    }


    std::istream& getlineStripCR(std::istream& input, std::string& s) {
        std::getline(input, s);
        stripCarriageReturn(s);
        return input;
    }

    std::string formatAutomicTimestamp(const std::string &in) {
        std::ostringstream ss;
        ss << in.substr(0, 4) << '-'   // year
           << in.substr(4, 2) << '-'   // month
           << in.substr(6, 2) << ' '   // day
           << in.substr(9, 2) << ':'   // hour
           << in.substr(11, 2) << ':'  // minute
           << in.substr(13,2) << '.'   // second
           << in.substr(16);           // millisecond
        return ss.str();
    }


}