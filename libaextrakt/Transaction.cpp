/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Transaction.h"

namespace uc4 {

    inline void Transaction::dump(size_t lvl) {
        string indent(lvl * 4, ' ');
        std::cout << indent
            << SR << "/" << name << " (" << eventId << ")"
            << " <-- " << returncode << std::endl;
        indent = string((lvl + 1) * 4, ' ');
        std::cout << indent << "Duration Total: " << durationTotal << ", DB: " << durationDB << std::endl;
        std::cout << indent << "Lines:          " << lineFrom << ":" << lineTo << std::endl;
    }

    // processed transaction
    void Transaction::printCSVheader(std::ostream & os) {
        os  << "sep=,\n"
               "filename,eventId,SR,xreq,name,"
               "senderProcess,senderDetail,senderSR,"
               "returncode,durationTotal,durationDB,"
               "beginTime,lineFrom,endTime,lineTo"
            << std::endl;
    }
    void Transaction::printCSV(std::ostream & os) {
        os << filename << "," << eventId << "," << SR << "," << xreq << "," << name << ","
            << senderProcess << "," << senderDetail << "," << senderSR << ","
            << returncode << "," << durationTotal << "," << durationDB << ","
            << timeFrom << "," << lineFrom << "," << timeTo << "," << lineTo
            << '\n';
    }



    // Send event
    void Transaction::printSendEventCSVheader(std::ostream& os) {
        os << "sep=,\n"
              "eventId,name,receiverSR,receiverProcess,"
              "queue,timestamp,eventDetail,client,"
              "cacv,bacv,type,prio,"
              "clientPrio,userid,"
              "senderEventid,senderSR,senderEventName,filename" << std::endl;
    }
    void Transaction::printSendEventCSV(std::ostream & os, const SendEventParser::Data & d) {
        std::ostringstream sender;
        std::ostringstream event;

        event << d.eventId << ',' << d.name << ',' << d.receiverSR << ',' << d.receiverProcess << ','
            << d.queue << ',' << d.timestamp << ',' << d.eventDetail << ',' << d.client << ','
            << d.cacv << ',' << d.bacv << ',' << d.type << ',' << d.prio << ','
            << d.clientPrio << ',' << d.userid;
        sender << eventId << ',' << SR << ',' << name << ',' << filename;

        os << event.str() << ',' << sender.str() << '\n';
    }




}