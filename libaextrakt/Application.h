/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include <vector>
#include <string>
#include <experimental/filesystem>
#include <boost/program_options.hpp>



namespace uc4 {

    namespace fs = std::experimental::filesystem;
    namespace po = boost::program_options;

    class Application
    {

    public:
        int validateArgs(int, const char**);
        int run(int argc, const char** argv);
        const fs::path& tracedir() { 
            return _tracedir;
        }
        const fs::path& outdir() {
            return _outdir;
        }
        static Application& instance() {
            static Application app;
            return app;
        }
        void reset() {
            _args.clear();
            _tracedir = ".";
            _outdir = "out";
        }


    private:
        std::vector<std::string> _args;
        fs::path _tracedir{ "." };
        fs::path _outdir{ "out" };
        Application() {}
    };

    void printHelp(po::options_description&);
    void printVersion();

}
