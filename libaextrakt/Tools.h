/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <functional>

#include <boost/lexical_cast.hpp>


namespace uc4 {

    constexpr long undefined = -2;
    template <class T> constexpr T nan() { return static_cast<T>(-1); }


    /// Remove trailing carriage return character
    /** This might happen when reading a Windows-textfile in binary mode */
    void stripCarriageReturn(std::string& s);

    /// Get line from input stream and strip trailing CR character.
    std::istream& getlineStripCR(std::istream& input, std::string& s);

    /**
     * Convert from Automic timestamp format to ISO 8601 like format
     * (' ' instead of 'T' between Date and Time)
     * @param in String in Automic timestamp format
     * @return Timestamp in ISO 8601 format "2016-12-24 20:15:00.123"
     */
    std::string formatAutomicTimestamp(const std::string &in);

    /// scope_exit as described in boost scope_exit alternatives
    /** http://www.boost.org/doc/libs/1_61_0/libs/scope_exit/doc/html/scope_exit/alternatives.html */
    struct scope_exit {
        explicit scope_exit(std::function<void(void)> f) : f_(f) {}
        ~scope_exit() { f_(); }
    private:
        std::function<void(void)> f_;
    };


    /// Numeric punctation preferences as used in tracefiles
    /** 2b used as Facet of std::locale */
    class numpunct : public std::numpunct<char> {
    protected:
        char_type do_decimal_point() const override { return ','; }
        char_type do_thousands_sep() const override { return '.'; }
    };

    /// RAII implementation for switching global locale (and back)
    class LocaleSwitcher {
    public:
        explicit LocaleSwitcher(const std::locale& newLocale) {
            std::locale::global(newLocale);
        }
        ~LocaleSwitcher() {
            std::locale::global(std::locale::classic());
        }
    private:
    };


    template<typename IntegerType>
    std::string toHex(IntegerType num) {
        std::stringstream stream;
        stream << std::setw(sizeof(IntegerType)*2) << std::setfill('0') << std::right << std::hex << num;
        return stream.str();


    }




    namespace internal {

				#pragma warning (disable : 4068 ) // suppress "unknown pragma" warning in VC++
				#pragma GCC diagnostic push
                #pragma GCC diagnostic ignored "-Wunused-parameter"
        template <typename ListIteratorType>
        size_t convertListEntries(ListIteratorType begin, ListIteratorType end, size_t& count) {
            return count;
        }
                #pragma GCC diagnostic pop

        template <typename ListIteratorType, typename part1_t, typename... parameters_t>
        size_t convertListEntries(ListIteratorType begin, ListIteratorType end, size_t& count, part1_t& part1, parameters_t&... parameters) {
            if (begin == end)
                return count;
            if (boost::conversion::try_lexical_convert<part1_t>(*begin, part1))
                return convertListEntries(++begin, end, ++count, parameters...);
            return count;
        }
    }

    /// Convert N entreis of STL like container to arbitrary type
    /** Container is filled with std::strings. Some may be formatted numerical values.
        You want to retrieven these entries as their real types.
        Usage:
            std::vector<std::string> myContainer{ "abcd", 666, 123.456, "unused" };
            std::string s; int i; double d;
            convertListEntreis( myContainer, s, i, d )
        @param list STL like container. Must have cbegin() and size() implemented.
        @param outputArgs Variable list of references to output variables where the converted value will be stored.
        @return true on successful convertion. false on conversion error
                or if more output parameters are specified than stored in container.
    **/
    template <typename ListType, typename... ArgsType>
    size_t convertListEntries(ListType& list, ArgsType&... outputArgs) {
        size_t count{ 0 };
        return internal::convertListEntries(list.begin(), list.end(), count, outputArgs...);
    }

    template <typename Target, typename Source>
    inline void lexcast( Target& target, const Source& source )
    {
        target = boost::lexical_cast<Target>( source );
    }


}
