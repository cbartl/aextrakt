/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <fstream>
#include <set>
#include <functional>
#include <tuple>
#include <cstdlib>

#include <boost/utility/string_view.hpp>
#include <boost/lexical_cast.hpp>

#include "Ucmsl.h"
#include "Tools.h"

namespace uc4
{


    using namespace std::placeholders;
    namespace fs = std::experimental::filesystem;
    using std::cerr;
    using std::regex;
    using std::bind;
    using std::not_equal_to;

    inline regex&
    Ucmsl::reCharactersToBeEscaped()
    {
        try
        {
            static string pattern{ R"___(([\\*+?^$()[\]{}|.]))___" };
            static regex  re{ pattern };
            return re;
        }
        catch ( ... )
        {
            std::cout << "Failed to construct regex reCharactersToBeEscaped. Let's get out of that stink." << std::endl;
            std::abort();
        }
    }

    inline regex&
    Ucmsl::reParamPlaceholders()
    {
        try
        {
            static regex re{ "<!parm!>" };
            return re;
        }
        catch ( ... )
        {
            std::cout << "Failed to construct regex reParamPlaceholders. Let's get out of that stink." << std::endl;
            std::abort();
        }
    }

    Ucmsl::Ucmsl( std::initializer_list<fs::path> candidates )
    {

        for ( auto cand : candidates )
        {
            if ( fs::is_directory( cand ) )
                cand /= ucmslfile;
            if ( fs::is_regular_file( cand ) )
            {
                file_ = cand;
                break;
            }
        }
        if ( file_.empty() )
            throw UcmslNotFoundException();

        is_ = std::make_unique<std::ifstream>( file_ );
    }

    Ucmsl::Ucmsl( std::unique_ptr<std::istream>&& is ) : is_( std::move( is ) ) {}


    bool
    Ucmsl::fileExists( const fs::path& file ) const
    {
        return (fs::exists( file ) && fs::is_regular_file( file ));
    }

    void
    Ucmsl::setNumDigits( const string& line )
    {
        numDigits_ = static_cast<decltype( numDigits_ ) > (
                std::distance( line.begin(),
                               std::find_if_not( line.begin(), line.end(), ::isdigit ) ));
    }

    void
    Ucmsl::load()
    {
        if ( !is_->good() )
            throw UcmslBadStreamException();

        string line;
        line.reserve( 1000 );
        std::istream& is{ *is_ };
        while ( getlineStripCR( is, line ) )
        {
            MslEntry entry{ parseLine( line ) };
            entries_[entry.language].entries.emplace_back( entry );
        }

        for ( auto& langEntries : entries_ )
        {
            auto& entries = langEntries.second.entries;
            std::sort( entries.begin(), entries.end(), []( auto& lhs, auto& rhs ) { return lhs.msgnr < rhs.msgnr; } );
        }
    }

    // 00000009DE'&01': Zugriff verweigert
    MslEntry
    Ucmsl::parseLine( const string& line )
    {
        using std::regex_error;

        MslEntry ret;
        if ( numDigits_ == 0 )
            setNumDigits( line );
        if ( line.length() < numDigits_ + 2 )
            return ret;

        string pattern{ ' ' };
        size_t maxMsgInsertIdx{ 0 };
        auto   currPos = numDigits_ + 2;
        while ( true )
        {
            // find candidate (starts with '&')
            auto candPos = line.find_first_of( '&', currPos );

            if ( candPos == line.npos )
            {
                pattern += line.substr( currPos );
                break;
            }

            // check if it's really a msginsert
            // - string-length
            if ( line.length() <= candPos + 2 )
            {
                pattern += line.substr( currPos );
                break;
            }
            // - format &00
            if ( !(::isdigit( line[candPos + 1] ) && ::isdigit( line[candPos + 2] )) )
            {
                pattern += line.substr( currPos, candPos - currPos + 1 );
                currPos = candPos + 1;
                continue;
            }

            // match, get the  number
            pattern += line.substr( currPos, candPos - currPos );
            auto msgInsertIdx = boost::lexical_cast<size_t>( line.data() + candPos + 1, 2 );
            ret.msgInsertIdx.push_back( msgInsertIdx );
            pattern += "<!parm!>";
            maxMsgInsertIdx   = std::max( maxMsgInsertIdx, msgInsertIdx );

            currPos = candPos + 3;
        }


        while ( ret.msgInsertIdx.size() < maxMsgInsertIdx )
            ret.msgInsertIdx.push_back( 0 );

        string pattern_new;
        string replace;
        string pattern_final;

        try
        {
            // escape special characters like '+' as "\+"
            replace = "\\$1";
            pattern_new.reserve( line.length() * 2 );
            regex_replace( std::back_inserter( pattern_new ), pattern.begin(), pattern.end(),
                           Ucmsl::reCharactersToBeEscaped(), replace );
            // add line start/end characters
            pattern_new[0] = '^';
            pattern_new += '$';

            // replace the placeholder <!param!> with regex group (.*)
            replace = "(.*)";
            pattern_final.reserve( pattern_new.capacity() );
            regex_replace( std::back_inserter( pattern_final ), pattern_new.begin(), pattern_new.end(),
                           reParamPlaceholders(),
                           replace );

            // finally fill return value
            ret.msgnr    = boost::lexical_cast<int>( line.substr( 0, numDigits_ ) );
            ret.language = Language( line[numDigits_] );
            ret.type     = Type( line[numDigits_ + 1] );
            ret.text     = line.substr( numDigits_ + 2 );
            ret.regex.assign( pattern_final );
        }
        catch ( boost::bad_lexical_cast& e )
        {
            cerr << "Cannot parse uc.msl MsgNr in line: \n. " << line;
            cerr << ". Error: " << e.what() << '\n';
        }
        catch ( regex_error& e )
        {
            cerr << "Regex error for uc.msl message number " << ret.msgnr << ", Language=" <<
                 static_cast< std::underlying_type<decltype( ret.language )>::type>(ret.language) << '\n';
            cerr << ". Bad Regex: '" << pattern_final << "'\n";
            cerr << ". Error: " << e.what() << '\n';
        }
        catch ( ... )
        {
            cerr << "Unknown error while parsing uc.msl line:\n. " << line << '\n';
        }
        return ret;
    }

    tuple<vector<string>, std::optional<string>>
    MslEntry::tryExtractMsgInserts( const string& fromText )
    const
    {
        using std::make_tuple;

        tuple<vector<string>, std::optional<string >> ret;
        auto & [msgInserts, errorText] = ret;

        std::smatch m;
        auto        search_ret         = regex_search( fromText, m, this->regex );
        if ( !search_ret )
        {
            std::ostringstream err;
            err << "Extract MsgInserts failed for message number "
                << msgnr
                << ", language="
                << static_cast<std::underlying_type<Language>::type>(language)
                << " for text: "
                << fromText << "\n";
            errorText = err.str();
            return ret;
        }

        // Check if match has expected number of groups
        if ( m.size() - 1 != static_cast<size_t>( std::count_if( msgInsertIdx.begin(), msgInsertIdx.end(),
                                                                 []( auto& item ) { return item != 0; } ) ) )
        {
            std::ostringstream err;
            err << "Extraced unexpected no. of MsgInserts for " << msgnr << ", language="
                << static_cast<std::underlying_type<Language>::type>(language)
                << ". Expected: "
                << msgInsertIdx.size() << ", Actual: " << m.size() - 1 << ". Affected text: " << fromText << "\n";
            errorText = err.str();
            return ret;
        }

        if ( m.size() <= 1 )
            // Not match found, but that's what we expected
            return ret;

        // Attention: content of msgInsertIdx start with index 1 (was &01 in uc.msl).
        // As smatch content starts with full match at m[0], the first matched group is
        // at m[1] ==> m[msgInserIdx[i]] does not require any calculations.
        msgInserts.resize( msgInsertIdx.size() );
        for ( size_t i = 0 ; i < msgInsertIdx.size() ; i++ )
            if ( msgInsertIdx[i] > 0 )
                msgInserts[msgInsertIdx[i] - 1] = m[i + 1];

        return ret;
    }

    tuple<vector<string>, optional<string>>
    Ucmsl::tryExtractMsgInserts( const long msgnr, const string& line, const Language language )
    const
    {
        using std::make_tuple;

        auto      & langEntries = entries_.at( language ).entries;
        const auto& entry       = std::find_if( langEntries.begin(), langEntries.end(),
                                                [msgnr]( const MslEntry& e ) -> bool { return e.msgnr == msgnr; } );

        if ( entry == langEntries.end() )
        {
            std::ostringstream err;
            err << "Cannot find msg number " << msgnr << " for language "
                << static_cast<std::underlying_type<Language>::type>(language);
            return make_tuple( vector<string>{}, err.str() );
        }

        return entry->tryExtractMsgInserts( line );
    }

    vector<string>
    Ucmsl::extractMsgInserts( const long msgnr, const string& line, const Language language )
    const
    {
        auto[ret, error] = tryExtractMsgInserts( msgnr, line, language );
        if ( error )
            throw MsgnrForLanguageNotFound( msgnr, language );
        return ret;
    }


    vector<string>
    Ucmsl::extractMsgInserts( long msgnr, const string& line, std::optional<Language>& language,
                              std::set<Language>& languageCandidates )
    const
    {
        vector<string> ret;

        if ( language )
            ret = extractMsgInserts( msgnr, line, *language );
        else
        {
            std::set<Language> toDelete;
            for ( Language     l : languageCandidates )
            {
                auto[inserts, error] = tryExtractMsgInserts( msgnr, line, l );
                if ( error )
                    toDelete.insert( l );
                else if ( ret.empty() )
                {
                    ret      = inserts;
                    language = l;
                }
            }
            for ( auto         l : toDelete )
                languageCandidates.erase( l );
        }
        return ret;
    }

}