/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include "FileInfo.h"
#include "Parsers.h"
#include "Tools.h"

#include <string>
#include <fstream>
#include <regex>
#include <memory>
#include <boost/utility/string_view.hpp>
#include <locale>


namespace uc4
{



    struct EventInfo;


    class Line
    {
    public:
        Line( const std::string& l, const FileInfo& fi ) : _line{ l }, _fi{ fi } {}

        const std::string&
        str() const { return _line; }

        long
        getMsgNr()
        {
            if ( _msgNr != undefined )
                return _msgNr;
            if ( _fi.msgnrDigits == 0 )
                return nan<long>();

            if ( _line.length() < _fi.contentBegin + _fi.msgnrDigits ) return -1;
            if ( _line[_fi.contentBegin] != 'U' ) return -1;
            boost::string_view smsg{ _line.data() + _fi.contentBegin + 1, _fi.msgnrDigits };
            std::locale        loc;
            for ( auto         c : smsg )
            {
                if ( !std::isdigit( c, loc ) )
                    return nan<long>();
            }
            return std::stol( smsg.to_string() );
        }


        /// If current line is begin of a hex trace line (U00009909) append the full
        /// hex trace to the string
        /// \param ifs The input stream we read the msg from
        /// \param hextrace Output string to store hex trace
        /// \return true if a hex trace was stored. false if line was something else
        bool
        processHexTrace( std::ifstream& ifs, std::string& hextrace )
        {
            if ( getMsgNr() != 9909 )
                return false;

            hextrace += _line;
            hextrace += '\n';
            std::streampos pos{ 0 };
            pos = ifs.tellg();
            std::string line;
            while ( getlineStripCR( ifs, line ) )
            {
                if ( line.length() < 1 || line[0] != ' ' )
                    break;
                hextrace += line;
                hextrace += '\n';
                pos = ifs.tellg();
            }
            // we read one line too much. Revert stream to stored pos.
            ifs.seekg( pos );
            return true;
        }

        bool
        tryReadSendEvent( EventInfo& );

        bool                         isSplit = false;
        optional<boost::string_view> timestamp;
        optional<long>               msgnr;
        optional<boost::string_view> text;

        void
        split()
        {
            auto ret = uc4::splitLine( _line, _fi.msgnrDigits );
            timestamp = get<0>( ret );
            msgnr     = get<1>( ret );
            text      = get<2>( ret );
        };


    private:
        const std::string& _line;
        const FileInfo   & _fi;
        long _msgNr{ undefined };
    };


    /// Content of "Send" line.
    struct EventInfo
    {
        std::string name;
        std::string senderSR;
        std::string receiverProcess;
        std::string eventDetail;
        std::string receiverSR;
        std::string queue;
        long        senderEventId{ uc4::nan<long>() };
        long        eventId{ uc4::nan<long>() };
        std::string bacv;
        std::string cacv;
        char        type{ '\0' };
        long        prio{ uc4::nan<long>() };
        long        clientPrio{ uc4::nan<long>() };
        long        client{ uc4::nan<long>() };
        long        userid{ uc4::nan<long>() };

        void
        clear()
        {
            name  = senderSR = receiverProcess = eventDetail = receiverSR = queue = "";
            eventId = prio     = clientPrio      = client      = userid     = uc4::nan<long>();
            type  = '\0';
            bacv.clear();
            cacv.clear();
        }
    };

}
