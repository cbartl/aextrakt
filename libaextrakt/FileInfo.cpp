/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <exception>
#include <stdexcept>
#include <algorithm>
#include <cctype>
#include <set>
#include <map>

#include "FileInfo.h"
#include "Tools.h"
#include "ParseFunctions.h"


namespace uc4
{

    void
    FileInfo::setMsgnrDigits( const std::string& line )
    {
        if ( msgnrDigits != 0 )
            return;                   // already set for this file. Assume the format doesn't change within a file.
        if ( line.length() < contentBegin + 8 ) return;   // Line too short, cannot contain a msg number


        std::istringstream ss( line );
        ss.seekg( std::streamoff( contentBegin ), ss.beg );

        char c;
        ss >> c;
        if ( c != 'U' ) return;       // msg number candidate must begin with 'U'

        std::string s;
        ss >> s;
        if ( s.length() < 7 || s.length() > 8 ) return;   // The word excl. 'U' must be 7-8 characters ...

        auto digits = static_cast <size_t> (std::count_if( s.begin(), s.end(), ::isdigit ));
        if ( digits != s.length() ) return;
        msgnrDigits = digits;
    }


    /// Set some information about the current file in a FileInfo
    /// \param An input stream of the file. Pos probably should be at the beginning.
    /// \return A FileInfo object containing information about the kind of process (WP, CP, JWP)
    ///         and whether this file was created by a trace change or process start.
    void
    getFileInfo( std::istream& input, FileInfo& _fi )
    {

        // save and restore stream pos
        const auto      savePos = input.tellg();
        uc4::scope_exit onExit( [&input, savePos]()
                                {
                                    input.seekg( savePos );
                                } );


        using std::string;
        using Trigger = FileInfo::Trigger;
        using Process = FileInfo::Process;
        string line;
        line.reserve( 1024 );


        std::map<long, string> msgnrs;

        for ( int i = 0 ; i < 10 ; i++ )
        {
            getlineStripCR( input, line );

            // we expect already first line contains msgnr
            _fi.setMsgnrDigits( line );
            if ( _fi.msgnrDigits == 0 )
                return;

            // get msgnr and text
            const auto&
            [optTimestamp, optMsgnr, optText] = _fi.splitLine( line );
            if ( !optTimestamp || !optMsgnr || !optText )
                continue;

            const auto& msgnr = *optMsgnr;
            if ( msgnr != nan<long>() )
                msgnrs[msgnr] = line;

            vector<string> msgInserts;
            switch ( msgnr )
            {
                case 3400:
                case 3380:
                case 3327:
                case 2000090:
                case 3450:
                case 3433:
                case 45067:
                    // extract msgInserts
                    msgInserts = _fi.ucmsl_.extractMsgInserts( msgnr, optText->to_string(), _fi.language,
                                                               _fi.languageCandidates_ );
                    break;
                default:continue;
            }

            switch ( msgnr )
            {
                case 3400:
                    // Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started.
                    // Used in many cases, but if it contains "#CP" we use this information
                    _fi.process         = msgInserts[0];
                    _fi.version         = msgInserts[1];
                    if ( _fi.process.find( "#CP" ) != string::npos )
                        _fi.processType = Process::CP;
                    break;

                case 3380:
                    // Server 'UC4#WP001' version '11.2.3+build.416' (Runtime '13/16:52:09', Log# '34', Trc# '146').
                    _fi.process         = msgInserts[0];
                    _fi.version         = msgInserts[1];
                    if ( _fi.process.find( "#CP" ) != string::npos )
                        _fi.processType = Process::CP;
                    else
                        _fi.processType = Process::WP;
                    _fi.trigger         = Trigger::TraceChange;
                    break;

                case 3327:
                {
                    // Build Date: '2016-10-14', '13:25:55'
                    std::ostringstream dateStream;
                    dateStream << msgInserts[0] << ' ' << msgInserts[1];
                    _fi.processType = Process::WP;
                    _fi.trigger     = Trigger::ProcStart;
                    _fi.buildDate   = dateStream.str();
                    break;
                }
                case 2000090:
                    // Java Runtime Environment version: '1.7.0_80'
                    _fi.jreVersion      = msgInserts[0];
                    _fi.processType     = Process::JWP;
                    if ( msgnrs.find( 3479 ) != msgnrs.end() )
                        _fi.trigger     = Trigger::TraceChange;
                    else
                        _fi.trigger = Trigger::ProcStart;
                    break;

                case 3450:
                    // The TRACE file was opened with the switches '2300000000000000'.
                    _fi.traceLevel = msgInserts[0];
                    break;

                case 3433:
                    // Server was started with INI file 'C:\_Automic\__ini\UCSrv.ini'
                    _fi.iniFile = msgInserts[0];
                    break;

                case 45067:
                    // Java installation directory: 'C:\Program Files\Java\jre1.8.0_101'
                    _fi.jreHome = msgInserts[0];
                    break;
            }

            if ( _fi.trigger != Trigger::Undefined && _fi.processType != Process::Undefined )
                break;
        }
    }


    long
    FileInfo::getMsgNr( const std::string& line )
    {
        long ret = uc4::getMsgNr( line, this->msgnrDigits );
        return ret;
    }

    tuple<optional<string_view>,
            optional<long>,
            optional<string_view>>
    FileInfo::splitLine( const std::string& line ) const
    {
        return uc4::splitLine( line, msgnrDigits );
    }

}