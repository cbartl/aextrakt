/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include "FileInfo.h"
#include "FilePos.h"
#include "Line.h"
#include "Ucmsl.h"

#include <experimental/filesystem>
#include <fstream>


namespace uc4
{

    namespace fs = std::experimental::filesystem;

    class File
    {
    public:
        File( const fs::path& path, std::ostream& transactionFile, std::ostream& messagesFile,
              std::ostream& fileInfoFile, const Ucmsl& ucmsl );

        File() = delete;

        int
        run();

        void
        printCSV();

        static void
        printCSVheader( std::ostream& );

        void
        doBeforeTransaction( Line& line );

        void
        processIniFile();

    private:
        std::ostream& _transactionFile;
        std::ostream& _eventsFile;
        std::ostream& _fileInfoFile;
        std::ifstream  _file;
        const fs::path _path;
        FileInfo       _fi;
        const Ucmsl& _ucmsl;
        FilePos _filePos;

    public:
        class TypeUnsupported : public std::runtime_error
        {
        public:
            TypeUnsupported()
                    : std::runtime_error( "Type of trace file not supported" ) {}

            ~TypeUnsupported() override = default;
        };

    };

}