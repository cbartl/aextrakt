/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once


//#include "Parsers.h"
#include <string>
#include <locale>
#include <sstream>
#include <fstream>
#include <set>
#include <optional>
#include <boost/utility/string_view.hpp>

#include "Ucmsl.h"

namespace uc4
{

    using std::string;


    struct DBInfo
    {
        int dummy;
    };

    struct FileInfo
    {
        size_t msgnrDigits{ 0 };
        size_t contentBegin{ 22 }; // "20161115/114655.971 - "

        string                  fileName;
        string                  process;
        string                  version;
        string                  buildDate;
        string                  iniFile;
        string                  OdbcConnectionString;
        string                  JdbcConnectionString;
        string                  hostName;
        string                  ipAddress;
        string                  traceLevel;
        string                  jreVersion;
        string                  jreHome;
        int64_t                 pid;
        string                  os;
        string                  cpu;
        int16_t                 cpuCount;
        int32_t                 memory;
        DBInfo                  dbInfo;
        int64_t                 perfCpu;
        int32_t                 perfDb;
        std::set<Language>      languageCandidates_{ Language::German, Language::English, Language::French };
        std::optional<Language> language;

        const Ucmsl& ucmsl_;

        FileInfo( const Ucmsl& ucmsl ) : ucmsl_{ ucmsl } {}

        FileInfo() = delete;


        /// By which event was the trace created
        /** ProcStart:   Trace was created by a process started with trace flags set in ini file
            TraceChange: Trace was created by a trace change during runtime of the process
        */
        enum class Trigger
        {
            Undefined, ProcStart, TraceChange
        }          trigger     = Trigger::Undefined;

        enum class Process
        {
            Undefined, WP, CP, JWP
        }          processType = Process::Undefined;

        string
        processTypeText()
        {
            string ret;
            switch ( processType )
            {
                case Process::CP: ret = "CP";
                    break;
                case Process::WP: ret = "WP";
                    break;
                case Process::JWP: ret = "JWP";
                    break;
                default: ret = "undef";
                    break;
            }
            return ret;
        }



        /// Check if this file uses Msg Numbers with 7 or 8 digits.
        /** Msg Numbers (U00003400) were changed from 7 to 8 digits somewhen. Try to find out if the current line
          * contains a msg number and if it has old or new format.
          * If a Msg Nr was found store the number of digits (excl. 'U') in this->msgnrDigits
          * @param line A single line of a trace file
          */
        void
        setMsgnrDigits( const std::string& line );

        long
        getMsgNr( const std::string& line );

        tuple<optional<boost::string_view>,
                optional<long>,
                optional<boost::string_view>>
        splitLine( const std::string& line ) const;

    };

    void
    getFileInfo( std::istream&, FileInfo& );

}