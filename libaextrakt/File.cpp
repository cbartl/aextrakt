/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wstrict-overflow"

// simpleini somhow included min/max macros of windows.h
#define NOMINMAX 

// from https://github.com/brofield/simpleini
// https://github.com/brofield/simpleini.git
#include "simpleini/SimpleIni.h"

#pragma GCC diagnostic pop

#include <regex>
#include <boost/algorithm/string.hpp>
#include <string>
#include <locale>
#include <stdexcept>
#include <optional>

#include "File.h"
#include "FilePos.h"
#include "Transaction.h"
#include "Line.h"
#include "Application.h"
#include "Parsers.h"
#include "Ucmsl.h"


namespace uc4
{

    using std::string;
    using std::optional;
    namespace fs = std::experimental::filesystem;



    File::File( const fs::path& pth, std::ostream& transactionFile, std::ostream& messagesFile,
                std::ostream& fileInfoFile, const Ucmsl& ucmsl )
            : _transactionFile( transactionFile )
              , _eventsFile( messagesFile )
              , _fileInfoFile( fileInfoFile )
              , _file( pth.string(), std::ifstream::binary )
              , _path( pth )
              , _fi( ucmsl )
              , _ucmsl( ucmsl )
    {
    }


    enum class TracePosition : std::uint8_t
    {
        beforeTransaction, inTransaction, betweenTransactions
    };


    void
    File::printCSVheader( std::ostream& os )
    {
        os << "sep=,\n"
                "filename,processType,version,buildDate,"
                "iniFile,OdbcConnectionString,JdbcConnectionString,"
                "hostName,ipAddress,pid,os,cpu,cpuCount,"
                "memory,perfCpu,perfDb,"
                "dbInfo"
           << std::endl;
    }

    void
    File::printCSV()
    {
        auto& os = _fileInfoFile;
        os << _fi.fileName << ',' << _fi.processTypeText() << ',' << _fi.version << ',' << _fi.buildDate << ','
           << _fi.iniFile << ',' << _fi.OdbcConnectionString << ',' << _fi.JdbcConnectionString << ','
           << _fi.hostName << ',' << _fi.ipAddress << ',' << _fi.pid << ',' << _fi.os << ',' << _fi.cpu << ','
           << _fi.cpuCount << ','
           << _fi.memory << ',' << _fi.perfCpu << ',' << _fi.perfDb << '\n' << std::flush;
    }


    void
    File::processIniFile()
    {
        // read ini file into memory
        std::ostringstream     iniFile;
        for ( optional<string> optLine = _filePos.nextLine() ;
              optLine.has_value() ;
              optLine       = _filePos.nextLine() )
        {
            string& sLine{ *optLine };
            if ( boost::ends_with( sLine, "----------" ) )
                break;



            auto[timestamp, msgNr, text] = splitLine( sLine, _fi.msgnrDigits );
            timestamp.reset();
            msgNr.reset();
            iniFile << *text << '\n';
        }

        // parse it with SimpleIni
        CSimpleIniA ini( false, false, false );
        string      iniString{ iniFile.str() };
        auto        retLoad = ini.LoadData( iniString.c_str(), iniString.size() );
        if ( retLoad != SI_OK )
        {
            std::cout << "Cannot process ini-file in trace. No big deal. Just some data unknown.";
            return;
        }

        // get values
        std::ostringstream odbc;
        odbc << '"' << ini.GetValue( "ODBC", "SQLDRIVERCONNECT", "" ) << '"';
        _fi.OdbcConnectionString = odbc.str();

        std::ostringstream jdbc;
        jdbc << '"' << ini.GetValue( "JDBC", "SQLDRIVERCONNECT", "" ) << '"';
        _fi.JdbcConnectionString = jdbc.str();
    }

    void
    File::doBeforeTransaction( Line& line )
    {
        _fi.setMsgnrDigits( line.str() );
        if ( !line.isSplit )
        {
            line.split();
            line.isSplit = true;
        }

        if ( line.msgnr )
        {
            // list of msg-numbers to extract
            vector<string> msgInserts;
            switch ( *line.msgnr )
            {
                case 3400:
                case 3327:
                case 3433:
                case 3492:
                case 3395:
                case 3328:
                case 3533:
                    msgInserts = _fi.ucmsl_.extractMsgInserts( *line.msgnr, line.text->to_string(), _fi.language,
                                                               _fi.languageCandidates_ );
                    break;
            }

            // use extracted msg-inserts
            switch ( *line.msgnr )
            {

                case 3400:
                    // Server 'SQL9_MY#WP' version '12.0.0+hf.1.build.3538' (changelist '7887925') started.
                    // Used in many cases, but if it contains "#CP" we use this information
                    _fi.process = msgInserts[0];
                    _fi.version = msgInserts[1];
                    break;

                case 3327:
                {
                    // Build Date: '2016-10-14', '13:25:55'
                    std::ostringstream dateStream;
                    dateStream << msgInserts[0] << ' ' << msgInserts[1];
                    _fi.buildDate = dateStream.str();
                    break;
                }
                case 3433:
                {
                    // Server was started with INI file 'C:\_Automic\__ini\UCSrv.ini'
                    _fi.iniFile = msgInserts[0];

                    // next comes the ini-file
                    auto optLine = _filePos.nextLine();
                    if ( optLine && boost::ends_with( *optLine, "----------" ) )
                        processIniFile();
                    break;
                }

                case 3492:
                    // U00003492 Server has been started on Host 'NB000445' ('192.168.216.97') with process ID '14936'.
                    _fi.hostName  = msgInserts[0];
                    _fi.ipAddress = msgInserts[1];
                    lexcast( _fi.pid, msgInserts[2] );
                    break;

                case 3395:
                {
                    // U00003395 Operating system 'Windows' version '6.1.7601 Service Pack 1' (build information '2016-10-14 13:25:55 - W64a1').
                    std::ostringstream osStream;
                    osStream << msgInserts[0] << ' ' << msgInserts[2] << " (" << msgInserts[1] << ')';
                    _fi.os = osStream.str();
                    break;
                }
                case 3328:
                {
                    // U00003328 The computer has '4' CPUs and '11974M' memory.
                    lexcast( _fi.cpuCount, msgInserts[0] );

                    string_view mem{ msgInserts[1] };
                    mem.remove_suffix( 1 );
                    lexcast( _fi.memory, mem );
                    break;
                }
                case 3533:
                    // U00003533 UCUDB: Check of data source finished: No errors. Performance CPU/DB: '45643848'/'1053'
                    _fi.perfCpu = boost::lexical_cast<int64_t>( msgInserts[0] );
                    _fi.perfDb  = boost::lexical_cast<int32_t>( msgInserts[1] );

            }
        }
    }


    int
    File::run()
    {
        std::cout << _path.string() << std::endl;
        _file.imbue( std::locale( std::cout.getloc(), new uc4::numpunct ) );


        getFileInfo( _file, _fi );
        switch ( _fi.processType )
        {
            case FileInfo::Process::WP:
            case FileInfo::Process::JWP:break;
            default:std::cout << "This is no tracefile. Skipping it.\n";
                return 1;
        }
        _fi.fileName = _path.filename().string();

        TracePosition                tracepos{ TracePosition::beforeTransaction };
        std::unique_ptr<Transaction> currTrans;
        string                       hextrace;
        std::ofstream                osTransaction;
        _filePos = FilePos{ &_file, 0 };

        TransactionBeginParser trBegin{ _fi };
        TransactionEndParser   trEnd{ _fi };
        SendEventParser        sendEvent{ _fi };

        for ( optional<string> optLine = _filePos.nextLine() ; optLine.has_value() ; optLine = _filePos.nextLine() )
        {
            string& sLine{ *optLine };
            Line line( sLine, _fi );

            try
            {
                switch ( tracepos )
                {
                    case TracePosition::beforeTransaction:
                    case TracePosition::betweenTransactions:
                    {
                        if ( tracepos == TracePosition::beforeTransaction )
                        {
                            doBeforeTransaction( line );
                            _fi.setMsgnrDigits( sLine );

                        }
                        if ( line.processHexTrace( _file, hextrace ) )
                            continue;
                        if ( trBegin.tryParse( sLine ) )
                        {
                            tracepos = TracePosition::inTransaction;
                            auto data{ trBegin.data() };
                            currTrans = std::make_unique<Transaction>( *data );
                            currTrans->lineFrom   = _filePos.lineNo;
                            currTrans->filename   = _fi.fileName;
                            currTrans->hexMessage = hextrace;

                            fs::path transactionFilename{ Application::instance().outdir() };
                            transactionFilename /= std::to_string( currTrans->eventId ) + "_" + currTrans->queue + ".txt";
                            std::ios_base::openmode openmode = std::ofstream::out;
                            if ( currTrans->eventId == 0 )
                                // append all events with id 0 into one file
                                openmode |= std::ofstream::app;
                            osTransaction.open( transactionFilename, openmode );
                            osTransaction << "\n" << currTrans->hexMessage << '\n';
                            osTransaction << sLine << '\n';
                        }
                        continue;
                    }
                    case TracePosition::inTransaction:
                    {
                        if ( osTransaction )
                            osTransaction << sLine << '\n';
                        if ( trEnd.tryParse( sLine ) )
                        {
                            auto data{ trEnd.data() };
                            currTrans->setTransactionEnd( *data, _filePos.lineNo );
                            currTrans->printCSV( _transactionFile );
                            osTransaction.close();
                            hextrace.clear();
                            tracepos = TracePosition::betweenTransactions;
                        }
                        else if ( sendEvent.tryParse( sLine ) )
                        {
                            auto d{ sendEvent.data() };
                            currTrans->printSendEventCSV( _eventsFile, *d );
                        }
                        continue;
                    }
                }
            }
            catch ( std::exception& e )
            {
                std::ostringstream txt;
                txt << "Exception while proccessing line " << _filePos.lineNo << "\n" << sLine << "\nDetails: "
                    << e.what();
                std::cout << txt.str() << std::endl;
                throw std::runtime_error( txt.str().c_str() );

            }
        }

        if ( _fi.processType != FileInfo::Process::Undefined )
            printCSV();

        return 0;
    }

}
