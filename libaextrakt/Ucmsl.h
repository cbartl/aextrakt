/* Copyright 2017 Christian Bartl

   This file is part of aextrakt, the Automation Engine trace extractor.

   aextrakt is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   aextrakt is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with aextrakt.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include <experimental/filesystem>
#include <map>
#include <set>
#include <regex>
#include <optional>
#include <sstream>

namespace uc4
{

    namespace fs = std::experimental::filesystem;
    using std::optional;
    using std::vector;
    using std::string;
    using std::tuple;

    enum class Language : char { English = 'E', German = 'D', French = 'F' };
    enum class Type : char
    {
        Info = 'I', Warning = 'W', Error = 'E', Fatal = 'F',
        A    = 'A', C = 'C', D = 'D'
    };

    struct MslEntry
    {
        long           msgnr;
        Language       language;
        Type           type;
        string         text;
        std::regex     regex;
        vector<size_t> msgInsertIdx;


        /// Extract the msgInserts from a trace line
        /// \param Trace line without timestamp and msg number.
        /// \return Tuple of list of msg inserts and optional error message if it fails
        tuple<vector<string>,
                optional<string>>
        tryExtractMsgInserts( const string& fromText ) const;
    };

    struct MslEntries
    {
        vector<MslEntry> entries;
    };

    class Ucmsl
    {
        const fs::path                ucmslfile{ "uc.msl" };
        size_t                        numDigits_{ 0 };
        fs::path                      file_;
        std::unique_ptr<std::istream> is_;

    public:

        std::map<Language, MslEntries> entries_{ { Language::English, {} },
                                                 { Language::German,  {} },
                                                 { Language::French,  {} } };

        explicit Ucmsl( std::initializer_list<fs::path> candidates );

        explicit Ucmsl( std::unique_ptr<std::istream>&& is );

        void
        load();


        /// Extract the msgInserts from a trace line
        /// \param Trace line without timestamp and msg number.
        /// \return Tuple of list of msg inserts and optional error message if it fails
        tuple<vector<string>, optional<string>>
        tryExtractMsgInserts( const long msgnr, const string& line, const Language language )
        const;

        vector<string>
        extractMsgInserts( const long msgnr, const string& line, const Language language )
        const;

        vector<string>
        extractMsgInserts( long msgnr, const string& line, optional<Language>& language,
                           std::set<Language>& languageCandidates )
        const;

    private:
        bool
        fileExists( const fs::path& file ) const;

        void
        setNumDigits( const string& line );

        MslEntry
        parseLine( const string& line );

        static std::regex& reCharactersToBeEscaped();
        static std::regex& reParamPlaceholders();
    };

    class UcmslNotFoundException : public std::runtime_error
    {
    public:
        UcmslNotFoundException()
                : std::runtime_error( "Cannot find uc.msl in In-dir, working-dir or application-dir" ) {}
    };

    class UcmslBadStreamException : public std::runtime_error
    {
    public:
        UcmslBadStreamException()
                : std::runtime_error( "Input-stream for uc.msl is not good." ) {}
    };

    struct MsgnrForLanguageNotFound : public std::runtime_error
    {
        explicit MsgnrForLanguageNotFound( const char* text ) : std::runtime_error( text ) {}

        MsgnrForLanguageNotFound( const long msgnr, const Language language ) : std::runtime_error( "" )
        {
            std::ostringstream s;
            s << "MsgNr " << msgnr << " not found for language '" << std::underlying_type<Language>::type( language )
              << "'.";
            (*this) = MsgnrForLanguageNotFound( s.str().c_str() );
        }
    };

}
